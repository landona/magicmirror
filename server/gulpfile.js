"use strict";

/**
 * Magic Mirror Build Script
 * Provides build and deployment automation for magic mirror
 * For more information run the help task
 */

var gulp = require('gulp-help')(require('gulp'), { hideEmpty: true, hideDepsMessage : true});
var gulpConfig = require("./gulp.config.js")();

var CONFIG = gulpConfig.CONFIG;
var CONST = gulpConfig.CONST;

//Import dependencies
var $ = require("gulp-load-plugins")({
    pattern: ["gulp-*", "gulp.*", "del", "event-stream", "minimist", "yargs"],
    lazy: true
});


//Retrieve Command Line Arguments
var argv = $.yargs.argv;

var iis = argv.iis != undefined;

var azure = argv.azure != undefined;
if (azure) {
    iis = true;
}


/*********************************************
 *                TASKS                      *
 *********************************************/


//Top
gulp.task("default", ["help"]);
gulp.task("build", CONST.BUILD_DESC, ["clean-dist"], function () { gulp.start("compile"); }, CONST.BUILD_OPTIONS);
gulp.task("build-docs", CONST.BUILD_DOCS_DESC, ["clean-docs"], generateDocs);
gulp.task("serve-docs", CONST.SERVE_DOCS_DESC, ["build-docs"], serveDocs);
gulp.task("sync", CONST.SYNC_DESC, sync);

//Clean
gulp.task("clean-dist", cleanDist);
gulp.task("clean-docs", cleanDocs);

//Compile
gulp.task("compile", ["compile-sass", "compile-server", "compile-client", "compile-lib-js", "compile-mirror-lib-css", "compile-messaging-lib-css", "compile-lib-font"], function () { gulp.start('inject'); });
gulp.task("compile-server", compileServer);
gulp.task("compile-client", compileClient);
gulp.task("compile-sass", compileMirrorSass);
gulp.task("compile-lib-js", compileLibJs);
gulp.task("compile-mirror-lib-css", compileMirrorLibCss);
gulp.task("compile-messaging-lib-css", compileMessagingLibCss);
gulp.task("compile-lib-font", compileMirrorLibFont);

//Inject
gulp.task("inject", ["inject-messaging-js", "inject-mirror-css"])
gulp.task("inject-mirror-css", ["inject-mirror-js"], injectMirrorCss);
gulp.task("inject-mirror-js", ["inject-mirror-lib-js"], injectMirrorJs);
gulp.task("inject-mirror-lib-js", injectMirrorLibJs);
gulp.task("inject-messaging-js", ["inject-messaging-lib-js"], injectMessagingJs);
gulp.task("inject-messaging-lib-js", ["inject-messaging-css"], injectMessagingLibJs)
gulp.task("inject-messaging-css", injectMessagingCss);


/*********************************************
 *             TASK DEFINITIONS              *
 *********************************************/


/**
 * Cleans / deletes contents of the distribution directory
 */
function cleanDist() {
    return $.del(CONFIG.DIST);
}

/**
 * Cleans / deletes contents of the docs directory
 */
function cleanDocs() {
    return $.del.sync([CONFIG.DOCS + "/**/*"]);
}

/**
 * Generates jsdoc style documents and deploys them to the docs directory
 */
function generateDocs() {
    var options = {
        html5Mode: false,
        title: "Magic Mirror API Documentation",
    }

    return gulp.src([CONFIG.BASE + "/public/**/*.js"])
        .pipe($.ngdocs.process(options))
        .pipe(gulp.dest(CONFIG.DOCS));
}

/**
 * Serves api documention from the docs directory
 */
function serveDocs() {
    $.connect.server({ root: CONFIG.DOCS });
}

/**
 * Synchronizes the javascript and html artifacts in the distribution with the ones in source
 */
function sync() {

    var sourceWatch = ["./**/*.js"];
    var templateWatch = ["./**/*.html"];

    $.watch(sourceWatch, { cwd: CONFIG.BASE }).on("change", function (file) { syncFile(file, CONFIG.BASE) });
    $.watch(templateWatch).on("change", function () { gulp.start("build"); });
}

/**
 * Migrates server files to the distribution. Optionally migrates iis
 * required files when the --iis option is specified
 */
function compileServer() {

    var src = [CONFIG.BASE + "/**/*", "!" + CONFIG.BASE + "/**/*.scss"];

    if (iis) {
        src.push("./web.config");
        src.push("./package.json");
    }

    return gulp.src(src)
        .pipe(gulp.dest(CONFIG.DIST));
}

/**
 * Migrates angular application assets to the distribution directory.
 */
function compileClient() {
    return gulp.src(["public/**/*", "!public/mirror/assets/css", "!public/mirror/assets/css/**", "!public/**/*.scss"])
        .pipe(gulp.dest(CONFIG.DIST + "/public"));
}

/**
 * Compiles sass for the mirror app and deploys the resultant css file to the distribution directory.
 */
function compileMirrorSass() { 
    return gulp.src(["./sass/**/*.scss", CONFIG.BASE + "/**/*.scss"])
        .pipe($.sass().on("error", $.sass.logError))
        .pipe($.concat("app.css"))
        .pipe(gulp.dest(CONFIG.DIST + "/public/mirror"));
}

/**
 * Concatenates npm managed javascript dependencies to a single file
 * and deploys to the distribution directory
 */
function compileLibJs() {
    return gulp.src(CONFIG.LIB_JS)
        .pipe($.concat('vendor.js'))
        .pipe(gulp.dest(CONFIG.DIST + "/public/mirror/assets/lib"))
        .pipe(gulp.dest(CONFIG.DIST + "/public/messaging/assets/lib"));
}

/**
 * Concatenates npm managed css mirror app dependencies to a single file
 * and deploys to the distribution directory, note that some of the 
 * library dependencies aren't managed by npm because they were either
 * customized, or don't exist in npm.
 */
function compileMirrorLibCss() {
    
    return compileLibCss(
        CONFIG.DIST + "/public/mirror/assets/css", 
        ["public/mirror/assets/css/*.css"]);
}

/**
 * Concatenates npm managed css messaging app dependencies to a single file
 * and deploys to the distribution directory
 */
function compileMessagingLibCss() {
    return compileLibCss(
        CONFIG.DIST + "/public/messaging/assets/css"
    );
}

/**
 * Migrates custom fonts to the distribution
 */
function compileMirrorLibFont() {
    return gulp.src(CONFIG.LIB_FONT)
        .pipe(gulp.dest(CONFIG.DIST + "/public/mirror/assets/font"));
}

/**
 * Dynamically injects css dependencies into mirror app html view(s)
 */
function injectMirrorCss() {
    return injectCss(
        gulp.src(CONFIG.DIST + "/public/mirror/**/*.css"),
        CONFIG.DIST + "/public/mirror/views/*.html",
        CONFIG.DIST + "/public/mirror/views",
        "css");
}

/**
 * Dynamically injects css dependencies into messaging app html view(s)
 */
function injectMessagingCss() {
    return injectCss(
        gulp.src(CONFIG.DIST + "/public/messaging/**/*.css"),
        CONFIG.DIST + "/public/messaging/*.html",
        CONFIG.DIST + "/public/messaging",
        "css");
}

/**
 * Dynamically injects library javascript dependencies into mirror app html view(s)
 */
function injectMirrorLibJs() {
    var sources = gulp.src([CONFIG.DIST + "/public/mirror/assets/lib/**/*.js"])
        .pipe($.angularFilesort());

    return injectJs(
        sources,
        CONFIG.DIST + "/public/mirror/views/*.html",
        CONFIG.DIST + "/public/mirror/views",
        "lib");
}

/**
 * Dynamically injects library javascript dependencies into messaging app html view(s)
 */
function injectMessagingLibJs() {
    var sources = gulp.src([CONFIG.DIST + "/public/messaging/assets/lib/**/*.js"])
        .pipe($.angularFilesort());

    return injectJs(
        sources,
        CONFIG.DIST + "/public/messaging/*.html",
        CONFIG.DIST + "/public/messaging",
        "lib");
}

/**
 * Dynamically injects application javascript dependencies into mirror app html view(s).
 * Angular files are sorted based on their dependencies
 */
function injectMirrorJs() {
    var sources = gulp.src([
            CONFIG.DIST + "/public/mirror/**/*.js",
            "!" + CONFIG.DIST + "/public/mirror/assets/lib/**",
            "!" + CONFIG.DIST + "/public/mirror/assets/lib/**/*.js"])
        .pipe($.angularFilesort());

    return injectJs(
        sources,
        CONFIG.DIST + "/public/mirror/views/*.html",
        CONFIG.DIST + "/public/mirror/views");
}

/**
 * Dynamically injects application javascript dependencies into messaging app html view(s).
 * Angular files are sorted based on their dependencies
 */
function injectMessagingJs() {

    var sources = gulp.src([
            CONFIG.DIST + "/public/messaging/**/*.js",
            "!" + CONFIG.DIST + "/public/messaging/assets/lib/**"])
        .pipe($.angularFilesort());

    return injectJs(
        sources,
        CONFIG.DIST + "/public/messaging/*.html",
        CONFIG.DIST + "/public/messaging"
    );
}


/*********************************************
 *        UTILITY / COMMON FUNCTIONS         *
 *********************************************/


/**
 * Dynamically injects application css dependencies.

 * @param {array} sources An array of relative paths to css dependencies
 * @param {string} target Target glob representing files to inject into
 * @param {string} dest Destination of the injected file
 * @param {string} name of inject tag in target for use with custom non-default tags
 */
function injectCss(sources, target, dest, name) {
    var transform = function (filePath, file) {
        return "<link rel='stylesheet' type='text/css' href='" + filePath + "'></link>";
    }

    return injectTarget(sources, target, dest, transform, name);
}

/**
 * Dynamically injects application js dependencies.

 * @param {array} sources An array of relative paths to js dependencies
 * @param {string} target Target glob representing files to inject into
 * @param {string} dest Destination of the injected file
 * @param {string} name of inject tag in target for use with custom non-default tags
 */
function injectJs(sources, target, dest, name) {

    var transform = function (filePath, file) {
        var scriptTag = "<script src='" + filePath + "'></script>";
        return scriptTag;
    };

    return injectTarget(sources, target, dest, transform, name);
}

/**
 * Dynamically injects application dependencies.

 * @param {array} sources An array of relative paths to dependencies
 * @param {string} target Target glob representing files to inject into
 * @param {string} dest Destination of the injected file
 * @param {function} transform The transformation function for the injection
 * @param {string} name of inject tag in target for use with custom non-default tags (optional)
 */
function injectTarget(sources, target, dest, transform, name) {

    var options = {
        transform: transform,
        addRootSlash: false,
        ignorePath: "dist"
    }

    if (name) {
        options["name"] = name;
    }

    return gulp.src(target)
        .pipe($.inject(sources, options))
        .pipe(gulp.dest(dest));
       
}

/**
 * Moves file from source to distribution
 * @param {string} file The full path to the source file
 */
function syncFile(file, base) {

    var path = CONFIG.DIST + "/" + file;

    path = path.replace(/\\/g, "/");
    path = path.substring(0, path.lastIndexOf("/") + 1);

    gulp.src(base+"/" + file)
        .pipe(gulp.dest(path));
}

/**
 * Concatenates npm managed css dependencies to a single file
 * and deploys to the distribution directory
 * @param {string} dest The distribution destination
 * @param {array} src an array of gulp globs representing the files to aggregate and move to the distribution
 */
function compileLibCss(dest, src) {

    var sources = CONFIG.LIB_CSS;
    if (src) {
        sources = sources.concat(src);
    }

    return gulp.src(sources)
        .pipe($.concat("vendor.css"))
        .pipe(gulp.dest(dest));
}
