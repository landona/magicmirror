var express = require("express");
var router = express.Router();
var path = require('path');
 
router.use("/photos", require("./components/photo/photo.routes"));
router.use("/compliment", require("./components/compliment/compliment.routes"))

router.use('/', express.static(__dirname + '/public/mirror'));
router.use('/public', express.static(__dirname + '/public'));

router.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/mirror/views/index.html'));
});

router.get('/demo', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/messaging/demo.html'));
});

router.get('/faceProvision', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/mirror/views/provisionFace.html'));
});

router.get('/photoViewer', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/mirror/views/photoViewer.html'));
});
 
module.exports = router;