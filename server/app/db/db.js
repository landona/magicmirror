var DocumentDBClient = require('documentdb').DocumentClient;
var DocumentBase = require('documentdb').DocumentBase;
var config = require("../config");
var ComplimentRepository = require("../components/compliment/compliment.repository");

//TODO: Only do this in dev / make this configurable...
var connectionPolicy = new DocumentBase.ConnectionPolicy();
connectionPolicy.DisableSSLVerification = true;

var docDbClient = new DocumentDBClient(
    config.db.host,
    {masterKey: config.db.authKey},
    connectionPolicy);

var complimentRepository = new ComplimentRepository(docDbClient);
complimentRepository.init(function (error) {
    console.log(error);
});

module.exports = { 
    complimentRepository: complimentRepository
};


