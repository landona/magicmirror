var express = require("express");
var router = express.Router();
var bodyParser = require('body-parser')
var Readable = require('stream').Readable;
var util = require('util');
var azure = require('azure-storage');
const uuidV1 = require('uuid/v1');
var fs = require('fs');

//TODO: THese should be configurable and should be in that storage class...
var STORAGE_ACCOUNT_NAME = "slalommagicmirrorstorage";
var STORAGE_ACCESS_KEY = "tuk6obxXwc5+XONvAFNyTOcPD3tuzEhDgY+Rf/FOcuQJWDNmEFALm5QOrOI6of1siF3krJZTqkTcuohSwaEkZQ==";
var STORAGE_CONTAINER_NAME = "mirrorstorage";

var parser = bodyParser.raw({ limit: '50mb', extended: true });

router.post("/", parser, function (req, res) {

    var ByteReadStream = function (bytes, options) {
        Readable.call(this, options);

        this.bytes = bytes;
        this.chunkSize = 1024;
        this.position = 0;
    };

    util.inherits(ByteReadStream, Readable);

    ByteReadStream.prototype._read = function (n) {

        if (this.position === this.bytes.length) {
            this.push(null);
        } else {
            var end = (this.position + this.chunkSize);
            end = end > this.bytes.length ? this.bytes.length : end;
            this.push(this.bytes.slice(this.position, end));
            this.position = end;
        }

    };

    var blobSvc = azure.createBlobService(STORAGE_ACCOUNT_NAME, STORAGE_ACCESS_KEY);

    var blobStream = new ByteReadStream(new Buffer(req.body));
    var blobName = uuidV1() + ".jpg";

    blobSvc.createBlockBlobFromStream(STORAGE_CONTAINER_NAME, blobName, blobStream, req.body.length, function (error, result, response) {
        if (!error) {
            blobSvc.getUrl(STORAGE_CONTAINER_NAME, blobName);

            res.json({ "status": "Success" });
        } else {
            //TODO: this should be returning some sort of error response, not 200 / Fail, this isn't Sharepoint 2010...
            res.json({ "status": "FAIL" });
        }
    });


})

router.get("/", function (req, res) {

    var blobSvc = azure.createBlobService(STORAGE_ACCOUNT_NAME, STORAGE_ACCESS_KEY);

    var url = blobSvc.getUrl(STORAGE_CONTAINER_NAME);

    blobSvc.listBlobsSegmented(STORAGE_CONTAINER_NAME, null, function (error, result, response) {

        var ret = [];

        if (!error) {

            if (result.entries && result.entries.length) {
                for (var i = 0; i < result.entries.length ; i++) {
                    var eachBlob = result.entries[i];

                    var eachRet = {
                        name: eachBlob.name,
                        path: url + "/" + eachBlob.name,
                        lastModified : eachBlob.lastModified
                    };

                    ret.push(eachRet);

                }
            } 
        }

        //TODO: there is no error handling here... :|
        res.json(ret);
    });

})


module.exports = router;