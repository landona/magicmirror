var DocumentDBClient = require('documentdb').DocumentClient;
var async = require('async');
var Util = require("../../util");
var Repository = require("../../db/repository");

var COLLECTION_ID = "compliment";

function ComplimentRepository(documentDBClient) {
    Repository.call(this, documentDBClient, COLLECTION_ID);
}

ComplimentRepository.prototype = {
    getCompliments : function() {

        //var self = this;

        //var querySpec = {
        //    query: 'SELECT * FROM root r WHERE r.completed=@completed',
        //    parameters: [{
        //        name: '@completed',
        //        value: false
        //    }]
        //};

        //self.find(querySpec, function (err, items) {
        //    if (err) {
        //        throw (err);
        //    }

        //    return items;
        //});

    },

    addCompliment: function (compliment) {
        
        var self = this;
         
        self.addItem(compliment, function (err) {
            if (err) { 
                throw (err);
            }
        });

    }
}

Util.extend(Repository, ComplimentRepository);

module.exports = ComplimentRepository;