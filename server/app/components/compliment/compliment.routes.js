var express = require("express");
var router = express.Router();

var db = require("../../db/db");
var storage = require("../../storage/storage");

router.post("/", function (req, res) {
    console.log(req.body);
    
    var repo = db.complimentRepository;

    repo.addCompliment(req.body);

    res.json({"status" :"Fantastic" }); 
})

router.get("/", function (req, res) {
    res.json({ "post": "Meh" });
}) 

module.exports = router;