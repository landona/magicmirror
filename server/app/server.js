var express = require("express");
var app = express();
var PubNub = require('pubnub');
var bodyParser = require("body-parser");
var config = require('./config');
var mysql = require('mysql');
var router = express.Router();
var builder = require('botbuilder');
var rest = require('./rest');
var moment = require('moment');

var connection = mysql.createConnection({
    host: 'us-cdbr-azure-east2-d.cloudapp.net',
    user: 'b7c9016b22c8f0',
    password: 'd5473cad',
    database: 'magicmirror',
    options: {
        encrypt: true
    }
});

connection.connect();

//app.use(bodyParser.urlencoded({ extended: false }));
app.use(require('./routes'));
//app.use(require('./bot'));

router.get('/mirrorapi/client-welcome-data', function (req, res) {
    var query = 'SELECT * FROM client_welcome';
    connection.query(query, function (err, rows) {
        if (err) {
            res.json({ "Error": true, "Message": "Error executing MySQL query", "error": err });
        } else {
            res.json({ "Error": false, "Message": "Success", "client_list": rows });
        }
    });

});

var connector = new builder.ChatConnector({
    appId: '0f0c471f-696b-4d50-8291-0c4af3c1b465',
    appPassword: '2ekCBYNya8PmmTFdYGveuow'
});

// Listen for messages from users 
router.post('/api/messages', connector.listen());

// Create your bot with a function to receive messages from the user
var bot = new builder.UniversalBot(connector, function (session) {
    var intent = session.message.text;
    var luisUrl = 'westus.api.cognitive.microsoft.com';
    var luisPath = '/luis/v2.0/apps/77e9b852-5e53-47d1-b41b-e0eac7521062?subscription-key=765ee366cff4439795286b464eb49cb2&verbose=true&timezoneOffset=0&q=' + encodeURIComponent(intent);
    var options = { host: luisUrl, path: luisPath, method: 'GET', headers: { 'Content-Type': 'application/json' } };

    rest.getJSON(options,
        function (statusCode, command) {
            if (command.intents.length) {
                var client = findEntityValue(command.entities, 'Client');
                var number = findEntityValue(command.entities, 'Number');
                var day = findEntityValue(command.entities, 'Day');
                var startTime = findEntityValue(command.entities, 'StartTime');
                var endTime = findEntityValue(command.entities, 'EndTime');
                var now = moment();
                var dbStart = moment(startTime.entity, "h : m").toDate();
                var dbEnd = moment(endTime.entity, "h : m").toDate();

                var post = { client_name: client.entity, start_time: dbStart, end_time: dbEnd };
                var query = connection.query('INSERT INTO client_welcome SET ?', post, function (error, results, fields) {
                    if (error) throw error;
                });

                session.send("Ok the welcome message will display for " + client.entity + " on " + day.entity + " at " + startTime.entity + " until " + endTime.entity);
            }
        });
});

function findEntityValue(entities, type) {
    return entities.filter(function (entity) {
        return entity.type === type;
    })[0];
}

app.use(router);

var server = app.listen(config.express.port, function () {
    var message = { "eventType": "ServerStarted", "config": config };
    var host = server.address().address;
    var port = server.address().port; 
    var pubnub = new PubNub({
        publishKey: 'pub-c-950f220c-9127-47e4-95db-23b9b36366cd',
        subscribeKey: 'sub-c-e937eed2-c169-11e6-963b-0619f8945a4f'
    });

    var publishConfig = {
        channel: "server_events",
        message: message
    }
    
    pubnub.publish(publishConfig, function (status, response) {
        console.log(status, response);
    });

    console.log('Magic Mirror listening at http://%s:%s', host, port);
});
