﻿var express = require("express");
var builder = require('botbuilder');
var rest = require('./rest');
var mysql = require('mysql');
var moment = require('moment');

//var luis = require('./public/mirror/shared/luis');
var botRouter = express.Router();

var connector = new builder.ChatConnector({
    appId: '0f0c471f-696b-4d50-8291-0c4af3c1b465',
    appPassword: '2ekCBYNya8PmmTFdYGveuow'
});

// Listen for messages from users 
botRouter.post('/api/messages', connector.listen());

// Create your bot with a function to receive messages from the user
var bot = new builder.UniversalBot(connector, function (session) {
    var intent = session.message.text;
    var luisUrl = 'westus.api.cognitive.microsoft.com';
    var luisPath = '/luis/v2.0/apps/77e9b852-5e53-47d1-b41b-e0eac7521062?subscription-key=765ee366cff4439795286b464eb49cb2&verbose=true&timezoneOffset=0&q=' + encodeURIComponent(intent);
    var options = { host: luisUrl, path: luisPath, method: 'GET', headers: { 'Content-Type': 'application/json' } };

    rest.getJSON(options,
        function (statusCode, command) {
            if (command.intents.length) {
                var client = findEntityValue(command.entities, 'Client');
                var number = findEntityValue(command.entities, 'Number');
                var day = findEntityValue(command.entities, 'Day');
                var startTime = findEntityValue(command.entities, 'StartTime');
                var endTime = findEntityValue(command.entities, 'EndTime');
                var now = moment();
                var dbStart = moment(startTime.entity, "h : m").toDate();
                var dbEnd = moment(endTime.entity, "h : m").toDate();
                var connection = mysql.createConnection({
                    host: 'us-cdbr-azure-east2-d.cloudapp.net',
                    user: 'b7c9016b22c8f0',
                    password: 'd5473cad',
                    database: 'magicmirror'
                });

                connection.connect();
                
                var post = { client_name: client.entity, start_time: dbStart, end_time: dbEnd  };
                var query = connection.query('INSERT INTO client_welcome SET ?', post, function (error, results, fields) {
                    if (error) throw error;
                });

                connection.end();

                session.send("Ok the welcome message will display for " + client.entity + " on " + day.entity + " at " + startTime.entity + " until " + endTime.entity);
            }
        });
});

function findEntityValue(entities, type) {
    return entities.filter(function (entity) {
        return entity.type === type;
    })[0];
}

module.exports = botRouter;