﻿var config = module.exports;
var PRODUCTION = process.env.NODE_ENV === "production";

config.express = {
  port: process.env.port || 1337
};

config.db = {
    host : process.env.DB_HOST || "https://127.0.0.1:1336",
    authKey : process.env.DB_AUTH_KEY || "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==",
    databaseId : "magic_mirror"
};

if (PRODUCTION) {
  // for example
  //config.express.ip = "0.0.0.0"
}

