(function () {
    'use strict'

    angular.module('demoApp', ['ngMaterial', 'ngAnimate', 'ngAria', 'pubnub.angular.service']);

})();