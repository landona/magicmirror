﻿(function () {
    'use strict'

    angular.module('demoApp').controller('DemoController', ['$scope', 'Pubnub', DemoController]);

    function DemoController($scope, Pubnub) {
        var vm = this;

        vm.uuid = Math.random(1000000).toString();
        vm.messageContent;
        vm.sendMessage = sendMessage;

        initialize();

        function initialize() {
            Pubnub.init({ publish_key: 'pub-c-950f220c-9127-47e4-95db-23b9b36366cd', subscribe_key: 'sub-c-e937eed2-c169-11e6-963b-0619f8945a4f' });
        }

        function sendMessage(form) {
            if (!vm.messageContent || vm.messageContent === '') {
                return;
            }

            Pubnub.publish({ channel: 'MagicMirror',
                             message: {
                                 content: JSON.stringify(vm.messageContent),
                                 sender_uuid: vm.uuid,
                                 date: new Date()
                             },
                             callback: function (m) {
                                 console.log(m);
                             }
            });

            form.$setPristine();
            //form.$setUntouched();
            form.message.$setPristine();
            //form.message.$setUntouched();
            vm.messageContent = '';
        }
    }
})();