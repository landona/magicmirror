﻿(function () {
    'use strict'

    angular.module('mirrorApp').controller('MirrorController', ['$scope', '$rootScope', '$window', '$timeout', 'app.service', 'luis.dataservice', 'Pubnub', 'session-storage', 'CONST', MirrorController]);

    function MirrorController($scope, $rootScope, $window, $timeout, appService, luis, Pubnub, session, CONST) {
        var vm = this;
        var facialRecognitionData = {};

        vm.uuid = Math.random(1000000).toString();
        vm.messages = [];
        vm.showLogo = true;
        
        vm.showVideoDisplay = false;
        vm.detectionInterval = CONST.PROP_FACIAL_DETECTIONINTERVAL;
        vm.defaultZipCode = CONST.MIRROR_DEFAULT_SETTINGS.WEATHER_DATA;
        vm.weatherRefreshRate = CONST.PROP_WEATHER_REFRESH_RATE;
        vm.trafficRefreshRate = CONST.PROP_TRAFFIC_REFRESH_RATE;
        vm.trafficApiKey = CONST.PROP_TRAFFIC_API_KEY;
        vm.startingCountDownValue = CONST.PROP_PHOTO_BOOTH_COUNTDOWN_VALUE;
        vm.delayTimeOut = CONST.PROP_PHOTO_BOOTH_REFRESH_RATE;
        vm.pauseForPicture = CONST.PROP_PHOTO_BOOTH_PAUSE_FOR_PICTURE;
        vm.faceDetectedEvent = CONST.EVENT_FACE_DETECTED;
        vm.faceConfidenceRating = CONST.PROP_FACIAL_CONFIDENCE_RATING;
        vm.blankMirrorEvent = CONST.EVENT_BLANK_MIRROR;
        vm.weatherApi = {};
        vm.photoBoothApi = {};
        vm.videoApi = {};
        vm.trafficApi = {};
        vm.clientApi = {};
        vm.fromAddress = CONST.MIRROR_DEFAULT_SETTINGS.FROM_LOCATION;
        vm.toAddress = CONST.MIRROR_DEFAULT_SETTINGS.TO_LOCATION;
        vm.clientList = [];

        vm.photoBoothFinished = photoBoothFinished;
        vm.faceFound = faceFound;
        vm.blankMirror = blankMirror;

        //REMOVE
        $scope.$on(CONST.COMMAND_HIDE_LOGO, handleHideLogo);
        $scope.$on(CONST.COMMAND_SHOW_LOGO, handleShowLogo);
        $scope.$on(CONST.COMMAND_TOGGLE_LOGO, handleToggleLogo);
        $scope.$on(CONST.COMMAND_SHOW_PHOTO_BOOTH, showPhotoBooth);
        $scope.$on('ChangeWeather', changeWeather);
        $scope.$on('ShowTrafficMap', showTrafficMap);
        $scope.$on('HideTrafficMap', hideTrafficMap);

        //////////
        initialize();

        function initialize() {
            setClientWelcome();
        }

        function changeWeather(event, command) {
            luis.getWeatherIntent(command.command).then(function (data) {
                vm.weatherApi.changeWeather(data.entities[0].entity);
            });
        }

        function showTrafficMap(event, command) {
            vm.trafficApi.toggleMap(true);
        }

        function hideTrafficMap(event, command) {
            vm.trafficApi.toggleMap(false);
        }

        function showPhotoBooth() {
            var mediaCapture = vm.videoApi.getMediaCapture();
            vm.showVideoDisplay = true;
            vm.photoBoothApi.startCountDown(mediaCapture);
        }

        function faceFound(faceData) {
            vm.facialRecognitionData = faceData;
            vm.weatherApi.changeWeather(faceData.location);
            vm.trafficApi.updateTraffic(vm.fromAddress, faceData.location);
        }

        function blankMirror() {
            vm.facialRecognitionData = {};
            vm.weatherApi.getWeather();
            vm.trafficApi.updateTraffic(vm.fromAddress, CONST.MIRROR_DEFAULT_SETTINGS.TO_LOCATION);
        }

        function setClientWelcome() {
            appService.clientList().then(function (data) {
                vm.clientApi.updateClientData(data.client_list);
                $timeout(setClientWelcome, CONST.PROP_WELCOME_REFRESH_RATE);
            });
        }
        //TODO: REMOVE
        function photoBoothFinished() {
            $rootScope.$broadcast(CONST.COMMAND_SHOW_WEATHER);
            $rootScope.$broadcast(CONST.COMMAND_SHOW_CLOCK);
            $rootScope.$broadcast(CONST.COMMAND_SHOW_COMPLIMENTS);
            $rootScope.$broadcast(CONST.COMMAND_SHOW_TRAFFIC);
            $rootScope.$broadcast(CONST.COMMAND_SHOW_LOGO);
            vm.showVideoDisplay = false;
        }

        function handleHideLogo() {
            vm.showLogo = false;
            $scope.$apply();
        }

        function handleShowLogo() {
            vm.showLogo = true;
            $scope.$apply();
        }

        function handleToggleLogo() {
            vm.showLogo = !vm.showLogo;
            $scope.$apply();
        }
    }

})();