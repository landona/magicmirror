﻿(function () {
    'use strict'

    angular.module('mirrorApp', ['ngMaterial', 'ngAnimate', 'ngAria', 'pubnub.angular.service'])

        .config(['$sceDelegateProvider', function ($sceDelegateProvider) {
            $sceDelegateProvider.resourceUrlWhitelist([
                //Allow same origin resource loads.
                'self',

                //Allow loading from Bing Maps Rest Services.
                'https://dev.virtualearth.net/REST/**'
            ]);
        }])

        .run(['pubNubService', 'voicePocService', "keyboardService", "keyboardInputHandler", function (pubNubService, voicePocService, keyboardService, keyboardInputHandler) {
            //Hack to grant keyboard focus to the app on Windows IOT, 
            //without this, the keyboard won't recieve DOM events from the document
            document.getElementById("focusHack").focus();
            document.getElementById("focusHack").blur();

            if (typeof Windows !== 'undefined') {
                var ViewManagement = Windows.UI.ViewManagement;
                var ApplicationViewWindowingMode = ViewManagement.ApplicationViewWindowingMode;
                var ApplicationView = ViewManagement.ApplicationView;
                ApplicationView.preferredLaunchWindowingMode = ApplicationViewWindowingMode.fullScreen;
            }
        }]);

})();