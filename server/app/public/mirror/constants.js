(function () {
    'use strict';

    angular.module('mirrorApp')
        .constant("CONST", {
            MIRROR_DEFAULT_SETTINGS: {
                FROM_LOCATION: '100 Pearl St Hartford, CT',
                TO_LOCATION: '821 2nd Ave, Seattle, WA',
                WEATHER_DATA: '89104'
            },
            MIRROR_STATE: {
                BLANK: 'Blank',
                ACTIVE: 'Active'
            },
            PROP_LOCALIZATION_SPEECH_RESOURCES: "LocalizationSpeechResources",
            PROP_CONSTRAINT_DICTATION: "dictation",
            PROP_FACIAL_DETECTIONINTERVAL: 500,
            PROP_FACIAL_CONFIDENCE_RATING: 0.5,
            PROP_WEATHER_REFRESH_RATE: 3600000,
            PROP_TRAFFIC_REFRESH_RATE: 3600000,
            PROP_TRAFFIC_API_KEY: 'nfsRUC33CamMI6yMUU0P~WM6HQXVOthuHWY_1ni50pg~AvXhS3C5OjcWSh4s8vV-VfW60zTZuvBud7VZPaPmtY94z01GKnI_Vk9UHggkIptD',
            PROP_PHOTO_BOOTH_COUNTDOWN_VALUE: 3,
            PROP_PHOTO_BOOTH_REFRESH_RATE: 1000,
            PROP_PHOTO_BOOTH_PAUSE_FOR_PICTURE: 2000,
            PROP_WELCOME_REFRESH_RATE: 600000, 

            COMMAND_HIDE_WEATHER: "COMMAND_HIDE_WEATHER",
            COMMAND_SHOW_WEATHER : "COMMAND_SHOW_WEATHER",
            COMMAND_TOGGLE_WEATHER: "COMMAND_TOGGLE_WEATHER",
            COMMAND_HIDE_CLOCK:"COMMAND_HIDE_CLOCK",
            COMMAND_SHOW_CLOCK:"COMMAND_SHOW_CLOCK",
            COMMAND_TOGGLE_CLOCK:"COMMAND_TOGGLE_CLOCK",
            COMMAND_SHOW_COMPLIMENTS: "COMMAND_SHOW_COMPLIMENTS",
            COMMAND_HIDE_COMPLIMENTS: "COMMAND_HIDE_COMPLIMENTS",
            COMMAND_TOGGLE_COMPLIMENTS: "COMMAND_TOGGLE_COMPLIMENTS",
            COMMAND_SHOW_DICTATION_LOG: "COMMAND_SHOW_DICTATION_LOG",
            COMMAND_HIDE_DICTATION_LOG: "COMMAND_HIDE_DICTATION_LOG",
            COMMAND_TOGGLE_DICTATION_LOG: "COMMAND_TOGGLE_DICTATION_LOG",
            COMMAND_HIDE_TRAFFIC: "COMMAND_HIDE_TRAFFIC",
            COMMAND_SHOW_TRAFFIC: "COMMAND_SHOW_TRAFFIC",
            COMMAND_TOGGLE_TRAFFIC: "COMMAND_TOGGLE_TRAFFIC",
            COMMAND_HIDE_CLOUD: "COMMAND_HIDE_CLOUD",
            COMMAND_SHOW_CLOUD: "COMMAND_SHOW_CLOUD",
            COMMAND_TOGGLE_CLOUD: "COMMAND_TOGGLE_CLOUD",
            COMMAND_HIDE_LOGO: "COMMAND_HIDE_LOGO",
            COMMAND_SHOW_LOGO: "COMMAND_SHOW_LOGO",
            COMMAND_TOGGLE_LOGO: "COMMAND_TOGGLE_LOGO",
            COMMAND_SHOW_PHOTO_BOOTH: "COMMAND_SHOW_PHOTO_BOOTH",
            COMMAND_PHOTOBOOTH_DONE: "COMMAND_PHOTOBOOTH_DONE",
             
            EVENT_LOG_LOG: "EVENT_LOG_LOG",
            EVENT_LOG_INFO: "EVENT_LOG_INFO",
            EVENT_LOG_WARN: "EVENT_LOG_WARN",
            EVENT_LOG_ERROR: "EVENT_LOG_ERROR",
            EVENT_LOG_DEBUG: "EVENT_LOG_DEBUG",
            EVENT_VOICE_STATE_CHANGED: "statechanged",
            EVENT_VOICE_RESULT_GENERATED: "resultgenerated",
            EVENT_VOICE_COMPLETED: "completed",
            EVENT_FACE_DETECTED: "EVENT_FACE_DETECTED",
            EVENT_BLANK_MIRROR: "EVENT_BLANK_MIRROR",

            WIN_MIC_ACCESS_DENIED: -2147024891,
            WIN_MIC_NONE_PRESENT: -1072845856,
            WIN_NO_MEDIA_REGISTERED: -2147221164,

            DEBUG_INITIATE_DICTATION: "Initiating dictation",
            DEBUG_STOPPING_DICTATION: "Stopping dictation",
            DEBUG_VOICE_IDLE: "Speech recognizer state: idle",
            DEBUG_VOICE_CAPTURING: "Speech recognizer state: capturing",
            DEBUG_VOICE_PROCESSING: "Speech recognizer state: processing",
            DEBUG_VOICE_SOUND_STARTED: "Speech recognizer state: soundStarted",
            DEBUG_VOICE_SOUND_ENDED: "Speech recognizer state: soundEnded",
            DEBUG_VOICE_SPEECH_DETECTED: "Speech recognizer state: speechDetected",
            DEBUG_VOICE_SPEECH_PAUSED: "Speech recognizer state: paused",
            DEBUG_VOICE_REJECTED_LOW_CONFIDENCE: "Discarded (low/rejected confidence): ",

            ERROR_INITIATE_FAILED: "Failed to initiate Dictation, Speech Recognizer in an invalid state.",
            ERROR_VOICE_GRAMMAR_CONSTRAINTS_FAILED: "Failed to compile grammar constraints",
            ERROR_MIC_ACCESS_DENIED: "Access denied (microphone disabled in settings)",
            ERROR_MIC_UNAVAILABLE: "Microphone Unavailable, check microphone privacy settings.",
            ERROR_MIC_NONE_PRESENT: "No Audio Capture devices are present on this system.",
            ERROR_MEDIA_PLAYER_UNAVAILABLE: "Media Player components not available on this system.",
            ERROR_VOICE_AUDIO_QUALITY_FAILURE: "Speech recognition error: audioQualityFailure",
            ERROR_VOICE_GRAMMAR_COMPILATION_FAILURE: "Speech recognition error: grammarCompilationFailure",
            ERROR_VOICE_GRAMMAR_LANGUAGE_MISMATCH: "Speech recognition error: grammarLanguageMismatch",
            ERROR_VOICE_MIC_UNAVAILABLE: "Speech recognition error: microphoneUnavailable",
            ERROR_VOICE_NETWORK_FAILURE: "Speech recognition error: networkFailure",
            ERROR_VOICE_PAUSE_LIMIT_EXCEEDED: "Speech recognition error: pauseLimitExceeded",
            ERROR_VOICE_TIMEOUT_EXCEEDED: "Speech recognition error: timeoutExceeded",
            ERROR_VOICE_TOPIC_LANGUAGE_NOT_SUPPORTED: "Speech recognition error: topicLanguageNotSupported",
            ERROR_VOICE_UNKNOWN: "Speech recognition error: unknown",
            ERROR_VOICE_CANCELED: "Recognition canceled by the user.",
            ERROR_TOTAL_DISASTER: "Computer divided by zero and went back in time."
        });
})();