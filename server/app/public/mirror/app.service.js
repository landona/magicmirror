﻿(function () {
    'use strict';

    angular.module('mirrorApp')
        .factory('app.service', AppService);

    AppService.$inject = ['$http', '$window'];

    function AppService($http, $window) {
        return {
            clientList: clientList
        };

        function clientList() {
            var url = $window.location.href + 'public/mirror/data/clients.json';
            return $http.get(url).then(function (response) {
                return response.data;
            }, function (error) {
                console.log(error);
            });
        }
    }
})();