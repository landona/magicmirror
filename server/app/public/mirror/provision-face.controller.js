﻿(function () {
    'use strict'

    angular.module('mirrorApp').controller('ProvisionFaceController', ['$q', 'facialrecognition.dataservice', 'CONST', ProvisionFaceController]);

    function ProvisionFaceController($q, service, CONST) {
        var vm = this;

        vm.detectionInterval = CONST.PROP_FACIAL_DETECTIONINTERVAL;
        vm.videoApi = {};

        vm.onSaveFace = onSaveFace;
        vm.provisionedFaces = [];
        vm.deleteFace = deleteFace;

        initialize();

        function initialize() {
            getFaceList();
        }

        function getFaceList() {
            service.getFaceList().then(function (response) {
                vm.provisionedFaces = response.persistedFaces.map(function (percistedFace) {
                    var userData = JSON.parse(percistedFace.userData);

                    userData.persistedFaceId = percistedFace.persistedFaceId;
                    return userData;
                });
            });
        }

        function deleteFace(persistedFaceId) {
            service.deleteFace(persistedFaceId).then(function () {
                getFaceList();
            });
        }

        function onSaveFace(personInfo) {
            var Storage = Windows.Storage;
            var stream = new Storage.Streams.InMemoryRandomAccessStream();

            takePhoto(vm.videoApi.getMediaCapture(), stream).then(
                function () {
                    var buffer = new Storage.Streams.Buffer(stream.size);

                    stream.seek(0);
                    stream.readAsync(buffer, stream.size, 0).done(function (stream) {
                        var dataReader = Storage.Streams.DataReader.fromBuffer(buffer);
                        var byteArray = new Uint8Array(buffer.length);

                        dataReader.readBytes(byteArray);

                        service.addFaceToList(byteArray, personInfo.desiredName, personInfo.location).then(
                            function (result) {
                                vm.desiredName = "";
                                vm.provisioning = false;
                                getFaceList();
                            },
                            function () {
                                vm.provisioning = false;
                            }
                        );
                    });
                },
                function () {
                    fr.provisioning = false;
                });
        }

        function takePhoto(mediaCapture, stream) {

            var deferred = $q.defer();

            mediaCapture.capturePhotoToStreamAsync(Windows.Media.MediaProperties.ImageEncodingProperties.createJpeg(), stream).then(function () {
                deferred.resolve();
            });

            return deferred.promise;
        }
    }

})();