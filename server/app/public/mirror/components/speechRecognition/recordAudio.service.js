(function () {
    "use strict";

    angular.module("mirrorApp")
        .factory("recordAudioService", ['$q', 'logger', recordAudioService]);

    function recordAudioService($q, log) {

        var service = {
            start: start,
            stop: stop
        };

        var capture;

        //TODO: Is this really the best way to do this?
        //Probably not...
        var ready = false;
        var recording = false;

        activate();

        return service;

        //////////

        function activate() {

            try {

                capture = new Windows.Media.Capture.MediaCapture();
                capture.initializeAsync().then(

                    function () {
                        ready = true;
                    },
                    function (error) {

                        log.error(error);

                    });

            } catch (exception) {
                log.error(exception);
            }

        }

        function start() {
            log.debug("Record start requested.");

            if (!ready || recording) {
                return log.error("Failed to begin recording, record service not initialized or already recording");
            }

            var localFolder = Windows.Storage.ApplicationData.current.localFolder;
            localFolder.createFileAsync("audio.mp3", Windows.Storage.CreationCollisionOption.generateUniqueName).then(
                function (file) {
                    var profile = Windows.Media.MediaProperties.MediaEncodingProfile.createMp3(Windows.Media.MediaProperties.AudioEncodingQuality.high);
                    capture.startRecordToStorageFileAsync(profile, file);

                    recording = true;

                    //setTimeout(function () {
                    //    capture.stopRecordAsync();

                    //    log.debug("Finishing...");
                    //    $scope.$apply();

                    //}, 60000);

                });
        }

        function stop() {
            log.debug("Record stop requested");

            if (!ready || !recording) {
                return log.error("Failed to stop recording, record service not initialized or already stopped");
            }

            capture.stopRecordAsync();
            recording = false;
        }

        function requestAudioCapturePermissions() {

            log.debug("Requesting Audio Capture Permissions");

            var deferred = $q.defer();

            try {

                // Only check microphone access for speech, we don't need webcam access.
                //var captureSettings = new Windows.Media.Capture.MediaCaptureInitializationSettings();
                //captureSettings.streamingCaptureMode = Windows.Media.Capture.StreamingCaptureMode.audio;
                //captureSettings.mediaCategory = Windows.Media.Capture.MediaCategory.speech;

                capture = new Windows.Media.Capture.MediaCapture();

                capture.initializeAsync(/*captureSettings*/).then(
                    function () {

                        log.debug("Capture initialized");

                        deferred.resolve();
                    },
                    function (error) {
                        // Audio Capture can fail to initialize if there's no audio devices on the system, or if
                        // the user has disabled permission to access the microphone in the Privacy settings.

                        if (error.number == CONST.WIN_MIC_ACCESS_DENIED) {

                            deferred.reject(CONST.ERROR_MIC_ACCESS_DENIED);

                        } else if (error.number == CONST.WIN_MIC_NONE_PRESENT) {

                            deferred.reject(CONST.ERROR_MIC_NONE_PRESENT);

                        } else {

                            log.error(error);
                            deferred.reject(CONST.ERROR_MIC_UNAVAILABLE);

                        }
                    }
                );

            } catch (exception) {
                if (exception.number == CONST.WIN_NO_MEDIA_REGISTERED) { // REGDB_E_CLASSNOTREG
                    deferred.reject(CONST.ERROR_MEDIA_PLAYER_UNAVAILABLE);
                } else {
                    deferred.reject(CONST.ERROR_MIC_UNAVAILABLE);
                }


            }

            return deferred.promise;
        }







    }

})();