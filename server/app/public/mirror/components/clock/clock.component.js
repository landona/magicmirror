﻿(function () {
    "use strict";

    angular.module("mirrorApp").component("clock", {
        templateUrl: "./components/clock/templates/clock.html",
        controller: ClockController,
        controllerAs: "vm",
        bindings: {
        }
    });

    ClockController.$inject = ["$scope", "$timeout", "CONST"];

    function ClockController($scope, $timeout, CONST) {

        var vm = this;

        vm.time = {};
        vm.date = {};
        vm.day = {};
        vm.show = true;

        $scope.$on(CONST.COMMAND_HIDE_CLOCK, handleHideClock);
        $scope.$on(CONST.COMMAND_SHOW_CLOCK, handleShowClock);
        $scope.$on(CONST.COMMAND_TOGGLE_CLOCK, handleToggleClock);

        initialize();

        //////////

        function initialize() {
            updateTime();
        }

        function updateTime() {
            var now = moment();

            vm.time = now.format("h:mm:ss");
            vm.date = now.format("MMMM D");
            vm.day = now.format("dddd");

            $timeout(updateTime, 500);
        }

        function handleHideClock() {
            vm.show = false;
            $scope.$apply();
        }

        function handleShowClock() {
            vm.show = true;
            $scope.$apply();
        }

        function handleToggleClock() {
            vm.show = !vm.show;
            $scope.$apply();
        }
    }

})();