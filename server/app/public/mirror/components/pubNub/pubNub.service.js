(function () {
    "use strict";

    angular.module('mirrorApp')
        .factory('pubNubService', ['$rootScope', '$window', 'logger', 'Pubnub', pubNubService]);

    function pubNubService($rootScope, $window, log, Pubnub) {
        
        var service = {};

        activate();

        return service;

        //////////

        function activate() {
            log.debug("pub nub activating");

            Pubnub.init({ publish_key: 'pub-c-950f220c-9127-47e4-95db-23b9b36366cd', subscribe_key: 'sub-c-e937eed2-c169-11e6-963b-0619f8945a4f' });

            Pubnub.subscribe({
                channel: 'MagicMirror',
                message: function (m) {

                    var message = m.content.replace(/\"/g, "");
                    
                    log.debug(message);

                    $rootScope.$broadcast('EVENT_PUB_NUB_MSG', message);

                },
                error: function (error) {
                    log.error(JSON.stringify(error));
                }
            });

            Pubnub.subscribe({
                channel: 'server_events',
                message: function (m) {

                    log.debug("Got command for restart!");

                    //TODO: fire off the event and handle in the main controller?
                    $window.location.reload();
                },
                error: function (error) {
                    log.error(JSON.stringify(error));
                }
            });
        }
    }

})();