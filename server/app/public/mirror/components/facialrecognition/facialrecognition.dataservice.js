﻿(function () {
    'use strict';

    angular.module('mirrorApp')
        .factory('facialrecognition.dataservice', FacialRecognitionDataService);

    FacialRecognitionDataService.$inject = ['$http', '$window'];

    function FacialRecognitionDataService($http, $window) {
        return {
            findFace: findFace,
            findSimilarFace: findSimilarFace,
            getFaceList: getFaceList,
            addFaceToList: addFaceToList,
            deleteFace: deleteFace
        };

        function getFaceList() {
            return $http.get('https://api.projectoxford.ai/face/v1.0/facelists/slalommirrorfaces', { headers: { 'Ocp-Apim-Subscription-Key': '976d63cbdbc446acac55f5002f7b0650' } }).then(function (response) {
                return response.data;
            }, function (error) {
                console.log(error);
            });
        }

        function findFace(byteArray) {
            return $http.post('https://api.projectoxford.ai/face/v1.0/detect', byteArray, { transformRequest: [], headers: { 'Content-Type': 'application/octet-stream', 'Ocp-Apim-Subscription-Key': '976d63cbdbc446acac55f5002f7b0650' }}).then(function (response) {
                return response.data;
            }, function (error) {
                console.log(error);
            });
        }

        function findSimilarFace(faceId) {
            var data = { faceId: faceId, faceListId: 'slalommirrorfaces' };

            return $http.post('https://api.projectoxford.ai/face/v1.0/findsimilars', data, { headers: { 'Ocp-Apim-Subscription-Key': '976d63cbdbc446acac55f5002f7b0650' } }).then(function (response) {
                return response.data;
            }, function (error) {
                console.log(error);
            });
        }

        function addFaceToList(byteArray, name, location) {
            var url = 'https://api.projectoxford.ai/face/v1.0/facelists/slalommirrorfaces/persistedFaces?&userData={"name":"' + name + '", "location":"' + location + '"}';

            return $http.post(url, byteArray, { transformRequest: [], headers: { 'Content-Type': 'application/octet-stream', 'Ocp-Apim-Subscription-Key': '976d63cbdbc446acac55f5002f7b0650' } });
            
        }

        function deleteFace(persistedFaceId) {
            var url = 'https://api.projectoxford.ai/face/v1.0/facelists/slalommirrorfaces/persistedFaces/' + persistedFaceId;

            return $http.delete(url, { headers: { 'Ocp-Apim-Subscription-Key': '976d63cbdbc446acac55f5002f7b0650' } }).then(function (response) {
                return response.data;
            }, function (error) {
                console.log(error);
            });
        }
    }
})();