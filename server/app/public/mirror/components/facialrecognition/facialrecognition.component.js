﻿(function () {
    'use strict';

    angular.module('mirrorApp').component('facialRecognition', {
        templateUrl: './components/facialrecognition/templates/facialrecognition.html',
        controller: FacialRecognitionController,
        controllerAs: 'vm',
        bindings: {
            facialDetectionEventName: '=',
            blankMirrorEventName: '=',
            faceConfidenceRating: '=',
            onFaceFound: '&',
            onBlankMirror: '&'
        }
    });

    FacialRecognitionController.$inject = ['facialrecognition.dataservice', '$scope', '$q', 'logger', 'CONST', '$timeout'];

    function FacialRecognitionController(service, $scope, $q, log, CONST, $timeout) {

        var checkingIdentity = false;
        var userFound = false;
        var photoBoothActive = false;
        var vm = this;

        vm.status;
        vm.name = '';
        vm.lastName = '';
        vm.cameraState = CONST.MIRROR_STATE.BLANK;

        vm.$onInit = function () {
            $scope.$on(vm.facialDetectionEventName, handleFace);
            $scope.$on(vm.blankMirrorEventName, blankMirror);
        }
        
        function handleFace(event, facialRecognitionData) {

            var Storage = Windows.Storage;
            var stream = new Storage.Streams.InMemoryRandomAccessStream();
            
            captureFace(facialRecognitionData.mediaCapture, stream).then(function () {
                var buffer = new Storage.Streams.Buffer(stream.size);

                stream.seek(0);
                stream.readAsync(buffer, stream.size, 0).done(function (stream) {
                    var dataReader = Storage.Streams.DataReader.fromBuffer(buffer);
                    var byteArray = new Uint8Array(buffer.length);

                    dataReader.readBytes(byteArray);

                    if (!checkingIdentity && !userFound) {
                        checkingIdentity = true;

                        service.findFace(byteArray).then(function (data) {
                            var faceId = data[0].faceId;
                            service.findSimilarFace(faceId).then(faceFound);
                        });
                    }
                });
            });
        }

        function faceFound(data) {
            var bestMatch = data[0];

            for (var i = 1; i < data.length; i++) {
                var currentUser = data[i];

                bestMatch = currentUser.confidence > bestMatch.confidence ? currentUser : bestMatch;
            }

            if (bestMatch.confidence >= vm.faceConfidenceRating) {

                log.debug("Face matched, confidence: " + bestMatch.confidence);

                service.getFaceList().then(function (faceList) {
                    faceList.persistedFaces.forEach(function (face) {
                        if (face.persistedFaceId == bestMatch.persistedFaceId) {
                            var faceData = JSON.parse(face.userData);
                            var phrase = (faceData.name === 'Jared Linde' ? 'Help me Jared Linde. You\'re my only hope.' : 'Hi ' + faceData.name + '. Looking mighty dapper!');

                            vm.name = phrase;
                            vm.cameraState = CONST.MIRROR_STATE.ACTIVE;
                            userFound = true;
                            vm.onFaceFound({ faceData: faceData });
                        }
                    });
                });
            }

        }

        function blankMirror() {
            checkingIdentity = false;
            userFound = false;
            vm.name = "";
            $scope.$apply();

            if (vm.cameraState !== CONST.MIRROR_STATE.BLANK) {
                vm.cameraState = CONST.MIRROR_STATE.BLANK;
                vm.onBlankMirror();
            }
        }

        function captureFace(mediaCapture, stream) {
            var deferred = $q.defer();

            mediaCapture.capturePhotoToStreamAsync(Windows.Media.MediaProperties.ImageEncodingProperties.createJpeg(), stream).then(function () {
                deferred.resolve();
            });

            return deferred.promise;
        }
    }
})();