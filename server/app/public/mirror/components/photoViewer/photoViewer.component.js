(function(){
    "use strict";

    angular
        .module('mirrorApp')
        .component('photoViewer', {
            templateUrl: './components/photoViewer/templates/photoViewer.html',
            controller: PhotoViewerController,
            controllerAs: "vm",
            bindings: {
            }
        });

    PhotoViewerController.$inject = ["$interval", "photoboothService"];

    function PhotoViewerController($interval, service) {
        var vm = this;

        vm.displayBuffer = new Array(2);
        vm.displayToggle = true;

        var photos = [];
        var index = 0;
        var position = 0;
        
        vm.$onInit = function () {

            $interval(cyclePhotos, 10000);
            $interval(refreshPhotos, 60000);

            service.getPhotos().then(
                function (result) {
                    if (result.data && result.data.length > 0){
                        photos = result.data;
                        vm.displayBuffer[0] = photos[0].path;
                        if (photos.length > 1) {
                            vm.displayBuffer[1] = photos[1].path;
                            index = 1;
                        } else {
                            vm.displayBuffer[1] = photos[0].path;
                        }
                    }
                }
            );
        }
        
        function cyclePhotos() {
            vm.displayToggle = !vm.displayToggle;

            position = position === 0 ? 1 : 0;
            index = index + 1 == photos.length ? 0 : index + 1;

            vm.displayBuffer[position] = photos[index].path;
        }

        function refreshPhotos() {
            service.getPhotos().then(
                function (result) {
                    if (result.data && result.data.length) {
                        if (result.data.length < photos.length) {
                            photos = result.data;

                            vm.displayBuffer[0] = photos[0].path;
                            if (photos.length > 1) {
                                vm.displayBuffer[1] = photos[1].path;
                                index = 1;
                            } else {
                                vm.displayBuffer[1] = photos[0].path;
                            }
                        } else {
                            photos = result.data;
                        }
                    } 
                }
            );
        }
    }

})();




