﻿(function () {
    'use strict';
    
    angular.module('mirrorApp').component('weather', {
        templateUrl: './components/weather/templates/weather.html',
        controller: WeatherController,
        controllerAs: "vm",
        bindings: {
            changeWeatherEvent: '=',
            refreshRate: '=',
            defaultZipCode: '=',
            registerHooks: '&',
            api: '='
        }
    });

    WeatherController.$inject = ['$scope', '$timeout', 'weather.dataservice', 'CONST', 'logger'];

    function WeatherController($scope, $timeout, service, CONST, log) {

        var vm = this;
        var iconTable = { '01d': 'wi-day-sunny', '02d': 'wi-day-cloudy', '03d': 'wi-cloudy', '04d': 'wi-cloudy-windy', '09d': 'wi-showers', '10d': 'wi-rain', '11d': 'wi-thunderstorm', '13d': 'wi-snow', '50d': 'wi-fog', '01n': 'wi-night-clear', '02n': 'wi-night-cloudy', '03n': 'wi-night-cloudy', '04n': 'wi-night-cloudy', '09n': 'wi-night-showers', '10n': 'wi-night-rain', '11n': 'wi-night-thunderstorm', '13n': 'wi-night-snow', '50n': 'wi-night-alt-cloudy-windy' };
        
        vm.show = true;
        vm.weatherData = {};
        vm.forecastData = {};
        
        vm.getWeatherIcon = getWeatherIcon;
        vm.convertDate = convertDate;
        vm.windDirections = windDirections;
        vm.setOpacity = setOpacity;

        $scope.$on(CONST.COMMAND_HIDE_WEATHER, handleHideWeather);
        $scope.$on(CONST.COMMAND_SHOW_WEATHER, handleShowWeather);
        $scope.$on(CONST.COMMAND_TOGGLE_WEATHER, handleToggleWeather);
        //////////

        vm.$onInit = function () {
            getWeather();
            vm.api = {
                changeWeather: changeWeather,
                getWeather: getWeather
            };
        };

        function getWeather() {
            service.getWeather(vm.defaultZipCode, 'dd8932ba99f08c22bf5274c7ecc1ecff').then(function (data) {
                vm.weatherData = data;
            });

            service.getForecast(vm.defaultZipCode, 'dd8932ba99f08c22bf5274c7ecc1ecff').then(function (data) {
                vm.forecastData = data;
            });

            $timeout(getWeather, vm.refreshRate);
        }

        function changeWeather(area) {
            service.getWeatherByArea(area, 'dd8932ba99f08c22bf5274c7ecc1ecff').then(function (data) {
                vm.weatherData = data;
            });

            service.getForecastByArea(area, 'dd8932ba99f08c22bf5274c7ecc1ecff').then(function (data) {
                vm.forecastData = data;
            });
        }

        function handleHideWeather() {
            vm.show = false;
            $scope.$apply();
        }

        function handleShowWeather() {
            vm.show = true;
        }

        function handleToggleWeather() {
            vm.show = !vm.show;
        }

        function getWeatherIcon(image) {
            return iconTable[image];
        }

        function convertDate(date, stringFormat) {
            return moment(date, "X").format(stringFormat);
        }
        
        function windDirections(deg) {
            if (deg > 11.25 && deg <= 33.75) {
                return "NNE";
            } else if (deg > 33.75 && deg <= 56.25) {
                return "NE";
            } else if (deg > 56.25 && deg <= 78.75) {
                return "ENE";
            } else if (deg > 78.75 && deg <= 101.25) {
                return "E";
            } else if (deg > 101.25 && deg <= 123.75) {
                return "ESE";
            } else if (deg > 123.75 && deg <= 146.25) {
                return "SE";
            } else if (deg > 146.25 && deg <= 168.75) {
                return "SSE";
            } else if (deg > 168.75 && deg <= 191.25) {
                return "S";
            } else if (deg > 191.25 && deg <= 213.75) {
                return "SSW";
            } else if (deg > 213.75 && deg <= 236.25) {
                return "SW";
            } else if (deg > 236.25 && deg <= 258.75) {
                return "WSW";
            } else if (deg > 258.75 && deg <= 281.25) {
                return "W";
            } else if (deg > 281.25 && deg <= 303.75) {
                return "WNW";
            } else if (deg > 303.75 && deg <= 326.25) {
                return "NW";
            } else if (deg > 326.25 && deg <= 348.75) {
                return "NNW";
            } else {
                return "N";
            }
        }

        function setOpacity(index) {

            var fadePoint = 0.25;            
            var startingPoint = vm.forecastData.list.length * fadePoint;
            var steps = 7 - startingPoint;

            if (index >= startingPoint) {
                var currentStep = index - startingPoint;
                return { 'opacity': 1 - (1 / steps * currentStep) };
            }
            
        }
    }

})();