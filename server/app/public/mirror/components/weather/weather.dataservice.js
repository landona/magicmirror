﻿(function () {

    angular.module('mirrorApp')
        .factory('weather.dataservice', ['$http', 'logger', WeatherDataService]);

    function WeatherDataService($http, logger) {
        return {
            getWeatherByArea: getWeatherByArea,
            getWeather: getWeather,
            getForecastByArea: getForecastByArea,
            getForecast: getForecast
        };

        function getWeatherByArea(area, appId) {
            return $http.get('http://api.openweathermap.org/data/2.5/weather?q=' + area + ',us&cnt=1&units=imperial&appid=' + appId)
                .then(responseData)
                .catch(serviceError);
        }

        function getWeather(zipCode, appId) {
            return $http.get('http://api.openweathermap.org/data/2.5/weather?zip=' + zipCode + ',us&cnt=1&units=imperial&appid=' + appId)
                .then(responseData)
                .catch(serviceError);
        }

        function getForecastByArea(area, appId) {
            return $http.get('http://api.openweathermap.org/data/2.5/forecast/daily?q=' + area + ',us&cnt=7&units=imperial&appid=' + appId)
                .then(responseData)
                .catch(serviceError);
        }

        function getForecast(zipCode, appId) {
            return $http.get('http://api.openweathermap.org/data/2.5/forecast/daily?zip=' + zipCode + ',us&cnt=7&units=imperial&appid=' + appId)
                .then(responseData)
                .catch(serviceError);
        }

        function responseData(response) {
            return response.data;
        }

        function serviceError(error) {
            logger.error(error.data);
        }
    }

})();