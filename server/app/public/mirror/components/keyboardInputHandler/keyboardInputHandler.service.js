(function () {
    "use strict";

    angular.module("mirrorApp")
        .factory("keyboardInputHandler", ["$rootScope", "CONST", "logger", KeyboardInputHandler]);

    function KeyboardInputHandler($rootScope, CONST, log) {

        var TOGGLE_CLOCK = 49;              //1
        var TOGGLE_WEATHER_BINDING = 50;    //2
        var TOGGLE_TRAFFIC = 51;            //3
        var TOGGLE_COMPLIMENTS = 52;        //4
        var TOGGLE_LOGO = 53;               //5
        
        var TOGGLE_CLOUD = 57;              //9
        var TOGGLE_DICTATION_LOG = 48;      //0

        var SHOW_PHOTO_BOOTH = 112;         //p

        var HIDE_ALL = 122;                 //z
        var SHOW_ALL = 120;                 //x

        $rootScope.$on("keypress", function (event, key) {

            log.debug("Key Pressed: " + key.which);

            switch (key.which) {
                case TOGGLE_WEATHER_BINDING:
                    $rootScope.$broadcast(CONST.COMMAND_TOGGLE_WEATHER);
                    break;
                case TOGGLE_CLOCK:
                    $rootScope.$broadcast(CONST.COMMAND_TOGGLE_CLOCK);
                    break;
                case TOGGLE_COMPLIMENTS:
                    $rootScope.$broadcast(CONST.COMMAND_TOGGLE_COMPLIMENTS);
                    break;
                case TOGGLE_DICTATION_LOG:
                    $rootScope.$broadcast(CONST.COMMAND_TOGGLE_DICTATION_LOG);
                    break;
                case TOGGLE_TRAFFIC:
                    $rootScope.$broadcast(CONST.COMMAND_TOGGLE_TRAFFIC);
                    break;
                case TOGGLE_CLOUD:
                    $rootScope.$broadcast(CONST.COMMAND_TOGGLE_CLOUD);
                    break;
                case TOGGLE_LOGO:
                    $rootScope.$broadcast(CONST.COMMAND_TOGGLE_LOGO);
                    break;

                case SHOW_PHOTO_BOOTH:
                    $rootScope.$broadcast(CONST.COMMAND_SHOW_PHOTO_BOOTH);
                    $rootScope.$broadcast(CONST.COMMAND_HIDE_WEATHER);
                    $rootScope.$broadcast(CONST.COMMAND_HIDE_CLOCK);
                    $rootScope.$broadcast(CONST.COMMAND_HIDE_COMPLIMENTS);
                    $rootScope.$broadcast(CONST.COMMAND_HIDE_TRAFFIC);
                    $rootScope.$broadcast(CONST.COMMAND_HIDE_LOGO);
                    break;

                case HIDE_ALL:
                    $rootScope.$broadcast(CONST.COMMAND_HIDE_WEATHER);
                    $rootScope.$broadcast(CONST.COMMAND_HIDE_CLOCK);
                    $rootScope.$broadcast(CONST.COMMAND_HIDE_COMPLIMENTS);
                    $rootScope.$broadcast(CONST.COMMAND_HIDE_TRAFFIC);
                    $rootScope.$broadcast(CONST.COMMAND_HIDE_LOGO);
                    break;
                
                case SHOW_ALL:
                    $rootScope.$broadcast(CONST.COMMAND_SHOW_WEATHER);
                    $rootScope.$broadcast(CONST.COMMAND_SHOW_CLOCK);
                    $rootScope.$broadcast(CONST.COMMAND_SHOW_COMPLIMENTS);
                    $rootScope.$broadcast(CONST.COMMAND_SHOW_TRAFFIC);
                    $rootScope.$broadcast(CONST.COMMAND_SHOW_LOGO);
                    break;
            }
        })

        return {};

    }

})();