﻿(function () {
    "use strict";

    angular.module("mirrorApp").component("traffic", {
        templateUrl: "./components/traffic/templates/traffic.html",
        controller: TrafficController,
        controllerAs: "vm",
        bindings: {
            refreshRate: '=',
            toAddress: '=',
            fromAddress: '=',
            trafficApiKey: '=',
            api: '='
        }
    });

    TrafficController.$inject = ["$scope", "traffic.dataservice", "$timeout", "CONST"];

    function TrafficController($scope, service, $timeout, CONST) {

        var vm = this;

        vm.show = true;
        vm.showMap = false;
        vm.trafficCongestion = {};
        vm.travelDuration = {};
        vm.mapUrl = '';

        $scope.$on(CONST.COMMAND_HIDE_TRAFFIC, handleHideTraffic);
        $scope.$on(CONST.COMMAND_SHOW_TRAFFIC, handleShowTraffic);
        $scope.$on(CONST.COMMAND_TOGGLE_TRAFFIC, handleToggleTraffic);

        vm.$onInit = function () {
            vm.api = {
                updateTraffic: updateTraffic,
                toggleMap: toggleMap
            };

            setTrafficDisplay();
        }

        function handleHideTraffic() {
            vm.show = false;
            $scope.$apply();
        }

        function handleShowTraffic() {
            vm.show = true;
            $scope.$apply();
        }

        function handleToggleTraffic() {
            vm.show = !vm.show;
            $scope.$apply();
        }

        function updateTraffic(fromAddress, toAddress) {
            vm.fromAddress = fromAddress;
            vm.toAddress = toAddress;
            setTrafficDisplay();
        }

        function setTrafficDisplay() {
            vm.mapUrl = 'http://dev.virtualearth.net/REST/v1/Imagery/Map/Road/Routes?wp.0=' + vm.fromAddress + ';64;1&wp.1=' + vm.toAddress + ';66;2&mapLayer=TrafficFlow&key=' + vm.trafficApiKey;

            service.getTraffic(vm.trafficApiKey, vm.fromAddress, vm.toAddress).then(function (data) {
                var congestion = data.resourceSets[0].resources[0].trafficCongestion;
                vm.trafficCongestion = congestion === "None" ? "No" : congestion;
                vm.travelDuration = data.resourceSets[0].resources[0].travelDurationTraffic / 60;
            });

            $timeout(setTrafficDisplay, vm.refreshRate);
        }

        function toggleMap(toggleValue) {
            vm.showMap = toggleValue;
        }
    }

})();