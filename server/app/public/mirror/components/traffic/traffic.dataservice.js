﻿(function () {

    angular.module('mirrorApp')
        .factory('traffic.dataservice', ['$http', 'logger', TrafficDataService]);

    function TrafficDataService($http, logger) {
        return {
            getTraffic: getTraffic
        };

        function getTraffic(trafficApiKey, fromAddress, toAddress) {
            var url = 'https://dev.virtualearth.net/REST/V1/Routes/Driving?wp.0=' + fromAddress + '&wp.1=' + toAddress + '&optmz=timeWithTraffic&key=' + trafficApiKey;
            return $http.jsonp(url, { jsonpCallbackParam: 'jsonp' })
                .then(responseData)
                .catch(serviceError);
        }

        function responseData(response) {
            return response.data;
        }

        function serviceError(error) {
            logger.error(error.data);
        }
    }

})();