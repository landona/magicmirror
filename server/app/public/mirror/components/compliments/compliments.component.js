(function () {
    "use strict";

    angular.module("mirrorApp").component("compliments", {
        templateUrl: "./components/compliments/templates/compliments.html",
        controller: ComplimentsController,
        controllerAs: "vm",
        bindings: {
        }
    });

    angular.module('mirrorApp')
    .filter('to_trusted', ['$sce', function ($sce) {
        return function (text) {
            return $sce.trustAsHtml(text);
        };
    }]);

    ComplimentsController.$inject = ["$interval", "$scope", "CONST", "$window"];

    function ComplimentsController($interval, $scope, CONST) {


        //TODO: If we don't have a config (probably a json file) make one and put this there...
        var UPDATE_INTERVAL = 15000;
        var FADE_SPEED = 4000;

        var lastComplimentIndex = -1;

        var vm = this;

        vm.show = true;

        vm.hideCompliments = false;
        vm.compliment = "";
        
        vm.hideComplete = hideComplete;

        $scope.$on(CONST.COMMAND_HIDE_COMPLIMENTS, handleHideCompliments);
        $scope.$on(CONST.COMMAND_SHOW_COMPLIMENTS, handleShowCompliments);
        $scope.$on(CONST.COMMAND_TOGGLE_COMPLIMENTS, handleToggleCompliments);

        //TODO: move these to the config too...
        var compliments = {
            morning: [
                "<div class='img-column'><img src='/assets/img/Award.png'></img></div><div class='img-column'><img src='/assets/img/Trophy.png'></img></div><div class='img-column'><img src='/assets/img/Cloud.png'></img></div>",
                //"&quot;Really cool deal! Love This! Thanks for the inspirational execution on this one!&quot; <div class='normal medium'>- Judson Althoff, EVP WW Commercial Business</div>",
                "<div style='font-style: italic;'>#LoveYourFuture</div>",
                "You've got a vision. Let's bring it to life.",
                "We bring an informed perspective from day one, immersing ourselves in your world and tackling the things that matter most to you and your customers.",
                "We help our clients reach for and realize their visions",
                "Our mission is to educate, connect, and inspire our clients and communities to capitalize on the full potential and business value of IoT.",
                "Ready to shape the future?",
                
                "Let's create beautiful software",

                "&quot;We exist to change the consulting experience for our employees and our clients. We simply believe that by treating our employees well and creating an excellent experience, including a great balance in their life, they will be even more focused on doing amazing work for our clients.&quot;<div class='normal medium'>- John Tobin Slalom Consulting</div>",

                //"#MorePuppies",
                //"Rizaelnig you can raed this mepsislled wlil be the hgihgliht of yuor day.",
                //"You need a mint. Like, bad.",
                //"You have such 60s airline stewardess hair!",
                //"I bet you make babies smile.",
                //"Your eyebrows are on fleek.",
                //"I like your outfit so much that if I wore it on the same day as you I wouldn't even be embarrassed, we'd just be that stylish.",
                //"I would hang out with you even if you hadn't showered for a couple days.",
                //"You actually have a lot of interesting things to say, and I don't say that about just anyone.",
                //"You deserve a promotion.",
                //"If laughter is the best medicine, your face must be curing the world.",
                //"You bring everyone a lot of joy, when you leave the room.",
                //"The simplicity of your character makes you exquisitely incomprehensible to me.",
                //"If looks could kill, you'd soon find out that yours couldn't.",
                //"Hi, you sexy BEAST (or BEAST-ESS)!",
                //"Motivation alone is not enough. If you have an idiot and you motivate him, now you have a motivated idiot",
                //"I'm actually not funny, I'm just mean and people think I'm joking.",
                //"Would you like to play a game?",
                "Simplicity is the ultimate sophistication #KISS",
                //"There's nothing so useless as doing something efficiently that should not be done at all.",
                //"If you do what you've always done, you'll get what you've always got.",
                //"Procrastination has taught me how to do 30 minutes worth of work in 8 hours, and 8 hours worth of work in 30 minutes.",
                //"The bureaucracy is expanding to meet the needs of the expanding bureaucracy.",
                //"An organization that treats its programmers as morons will soon have programmers that are willing and able to act like morons only.",
                "Do the planning but throw out the plans.",
                //"When you're about to screw up, don't...",
                "Ironing boards are just surfboards that gave up on their dreams and got a boring job, don't be an ironing board.",
                "Preparing to divide by zero",
                "Please don't touch me, I don't like smudges...",
                "Hello Dave..."
            ],
            afternoon: [
                "#MorePuppies",
                "Rizaelnig you can raed this mepsislled wlil be the hgihgliht of yuor day.",
                "You need a mint. Like, bad.",
                "You have such 60s airline stewardess hair!",
                "I bet you make babies smile.",
                "Your eyebrows are on fleek.",
                "I like your outfit so much that if I wore it on the same day as you I wouldn't even be embarrassed, we'd just be that stylish.",
                "I would hang out with you even if you hadn't showered for a couple days.",
                "You actually have a lot of interesting things to say, and I don't say that about just anyone.",
                "You deserve a promotion.",
                "If laughter is the best medicine, your face must be curing the world.",
                "You bring everyone a lot of joy, when you leave the room.",
                "The simplicity of your character makes you exquisitely incomprehensible to me.",
                "If looks could kill, you'd soon find out that yours couldn't.",
                "Hi, you sexy BEAST (or BEAST-ESS)!",
                "Motivation alone is not enough. If you have an idiot and you motivate him, now you have a motivated idiot",
                "I'm actually not funny, I'm just mean and people think I'm joking.",
                "Would you like to play a game?",
                "Simplicity is the ultimate sophistication #KISS",
                "There's nothing so useless as doing something efficiently that should not be done at all.",
                "If you do what you've always done, you'll get what you've always got.",
                "Procrastination has taught me how to do 30 minutes worth of work in 8 hours, and 8 hours worth of work in 30 minutes.",
                "The bureaucracy is expanding to meet the needs of the expanding bureaucracy.",
                "An organization that treats its programmers as morons will soon have programmers that are willing and able to act like morons only.",
                "Do the planning but throw out the plans.",
                "When you're about to screw up, don't...",
                "Ironing boards are just surfboards that gave up on their dreams and got a boring job, don't be an ironing board.",
                "Preparing to divide by zero",
                "Please don't touch me, I don't like smudges...",
                "Hello Dave..."/*,
                "<- And they told me that size doesn't matter :("*/
            ],
            evening: [
                "You're still here? Go home...",
                "#MorePuppies"
            ]
        };

        initialize();

        //////////

        function initialize() {

            updateCompliment();
            $interval(updateCompliment, UPDATE_INTERVAL);
            
        }


        function handleHideCompliments() {
            vm.show = false;
            $scope.$apply();
        }

        function handleShowCompliments() {
            vm.show = true;
            $scope.$apply();
        }

        function handleToggleCompliments() {
            vm.show = !vm.show;
            $scope.$apply();
        }


        function hideComplete() {
            var compliments = complimentArray();
            vm.compliment = compliments[nextIndex(compliments)];
            vm.hideCompliments = false;
        }

        function updateCompliment() {
            vm.hideCompliments = true;
        }

        function nextIndex(compliments) {
            if (compliments.length === 1) {
                return 0;
            }

            lastComplimentIndex = lastComplimentIndex == -1 ? 0 : lastComplimentIndex;

            return lastComplimentIndex == compliments.length ? lastComplimentIndex = 0 : lastComplimentIndex++;
        }

        function randomIndex (compliments) {
            if (compliments.length === 1) {
                return 0;
            }

            var generate = function() {
                return Math.floor(Math.random() * compliments.length);
            };

            var complimentIndex = generate();

            while (complimentIndex === lastComplimentIndex) {
                complimentIndex = generate();
            }

            lastComplimentIndex = complimentIndex;

            return complimentIndex;
        }

        function complimentArray() {
            return compliments.morning;

            var hour = moment().hour();

            if (hour >= 3 && hour < 12) {
                return compliments.morning;
            } else if (hour >= 12 && hour < 17) {
                return compliments.afternoon;
            } else {
                return compliments.evening;
            }
        }

    }
})();