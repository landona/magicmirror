(function () {
    "use strict";

    angular.module("mirrorApp")

        .component('logViewer', {
            templateUrl: './components/logViewer/templates/logViewer.html',
            controller: LogViewerController,
            controllerAs: "vm",
            bindings: {
            }
        });

    LogViewerController.$inject = ['$scope', 'CONST'];

    function LogViewerController($scope, CONST) {

        var vm = this;

        vm.log = "";

        $scope.$on(CONST.EVENT_LOG_DEBUG, handleLogEvent);
        $scope.$on(CONST.EVENT_LOG_ERROR, handleLogEvent);

        //////////

        function handleLogEvent(event, msg) {
            log(msg);
        }

        function log(msg) {
            vm.log = msg + "\n" + vm.log;
            $scope.$apply();
        }
    }

})();