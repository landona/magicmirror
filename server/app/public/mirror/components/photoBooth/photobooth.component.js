﻿(function () {
    'use strict';

    angular.module('mirrorApp').component('photoBooth', {
        templateUrl: './components/photoBooth/templates/photoBooth.html',
        controller: PhotoBoothController,
        controllerAs: "vm",
        bindings: {
            startingCountDownValue: '=',
            delayTimeOut: '=',
            pauseForPicture: '=',
            photoTaken: '&',
            api: '='
        }
    });

    PhotoBoothController.$inject = ['$scope', '$q', '$timeout', 'logger', 'CONST', "photoboothService"];

    function PhotoBoothController($scope, $q, $timeout, log, CONST, service) {
    
        var vm = this;

        //TODO: This should be in the config
        var DELAY = 1000;
        var oMediaCapture;
        var inProgress = false;

        vm.status;
        vm.showPhotoBooth = false;
        
        vm.$onInit = function () {
            vm.api = {
                startCountDown: startCountDown
            };
        };
        
        function startCountDown(mediaCapture) {

            if (!inProgress) {
                inProgress = true;
                log.debug("Showing Photo Booth");
                oMediaCapture = mediaCapture;
                $timeout(function () { updateCountDown(vm.startingCountDownValue) }, vm.delayTimeOut);
            }
        }

        function updateCountDown(countDown) {
            if (countDown === 0) {
                vm.status = 'Say Cheese!';
                $timeout(takePhoto, vm.pauseForPicture);
            } else {
                vm.status = countDown;
                $timeout(function () { updateCountDown(--countDown) }, vm.delayTimeOut);
            }
        }

        function takePhoto() {
            var inputStream;

            var Streams = Windows.Storage.Streams;
            inputStream = new Streams.InMemoryRandomAccessStream();

            oMediaCapture.capturePhotoToStreamAsync(Windows.Media.MediaProperties.ImageEncodingProperties.createJpeg(), inputStream).then(
                function () {

                    log.debug("Took a picture...");

                    var Storage = Windows.Storage;
                    var buffer = new Storage.Streams.Buffer(inputStream.size);

                    inputStream.seek(0);

                    log.debug("Dumping picture into buffer");

                    inputStream.readAsync(buffer, inputStream.size, 0).done(function (inputStream) {

                        //TODO, this is a potential memory leak...
                        try { inputStream.close(); } catch (e) { }

                        var dataReader = Storage.Streams.DataReader.fromBuffer(buffer);
                        var byteArray = new Uint8Array(buffer.length);

                        dataReader.readBytes(byteArray);
                        
                        service.uploadPhoto(byteArray).then(
                            function () {
                                log.debug("Photo uploaded succesfully");
                                vm.status = "";
                                vm.photoTaken();
                            },
                            function (error) {
                                log.debug("Failed to upload photo");
                                log.error(error);
                            }
                        ).finally(function() {
                            inProgress = false;
                        });

                    });
                },
                function (error, two) {
                    log.error(error);
                    log.error(two);
                }
            );
        }
    }
})();