(function () {
    "use strict";

    angular
        .module("mirrorApp")
        .factory("photoboothService", ["$window", "$http", PhotoboothService]);

    function PhotoboothService($window, $http) {
        
        var service = {
            uploadPhoto: uploadPhoto,
            getPhotos:getPhotos
        };

        return service;

        function uploadPhoto(byteArray) {
            var dest = $window.location.href + "photos";
            return $http.post(dest, byteArray, { transformRequest: [], headers: { 'Content-Type': 'application/octet-stream' } });
        }

        function getPhotos() {
            var dest = $window.location.origin + "/photos";

            return $http.get(dest);
        }

    }

})();




