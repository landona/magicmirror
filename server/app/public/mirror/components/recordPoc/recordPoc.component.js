(function () {
    "use strict";

    angular.module('mirrorApp')
        .component('recordPoc', {
            templateUrl: './components/recordPoc/templates/recordPoc.html',
            controller: RecordPocController,
            controllerAs: "vm",
            bindings: {
            }
        });

    RecordPocController.$inject = ['$scope', '$q', 'CONST', 'logger'];

    function RecordPocController($scope, $q, CONST, log) {

        var vm = this;
        
        //TODO: Is this really the best way to do this?
        //Probably not...
        var ready = false;
        var recording = false;

        var capture;

        //var capture;

        $scope.$on(CONST.EVENT_LOG_DEBUG, handleLogEvent);
        $scope.$on(CONST.EVENT_LOG_ERROR, handleLogEvent);
        activate();


        function activate() {

            //log.debug("Activating RecordPocController");

           // requestAudioCapturePermissions().then(

                //function () {

                    //log.debug("Permissions succesfully obtained");

                    try {

                        capture = new Windows.Media.Capture.MediaCapture();
                        capture.initializeAsync().then(

                            function () {
                                ready = true;
                            },
                            function(error){
                                
                                log.error(error);
                            
                            });

                        //capture.prepareLowLagRecordToStorageFileAsync(profile,file).done(
                        //    function () {
                        //        log.debug("Preparing to record...");
                        //        capture.startRecordToStorageFileAsync(profile, file);

                        //        log.debug("Recording");
                        //        $scope.$apply();

                        //        setTimeout(function(){
                        //            capture.stopRecordAsync();

                        //            log.debug("Finishing...");
                        //            $scope.$apply();

                        //        }, 3000);

                        //    },
                        //    function () {
                        //        log.debug("Not Recording");
                        //    })


                    } catch (exception) {
                        log.error(exception);
                    }
                    var i = 0;

               // });
        }

        function record() {
            log.debug("Record start requested.");

            if (!ready) {
                return log.error("Failed to begin recording, record service not initialized");
            }

            var localFolder = Windows.Storage.ApplicationData.current.localFolder;
            localFolder.createFileAsync("audio.mp3", Windows.Storage.CreationCollisionOption.generateUniqueName).then(
                function (file) {
                    var profile = Windows.Media.MediaProperties.MediaEncodingProfile.createMp3(Windows.Media.MediaProperties.AudioEncodingQuality.high);
                    capture.startRecordToStorageFileAsync(profile, file);

                    setTimeout(function () {
                        capture.stopRecordAsync();

                        log.debug("Finishing...");
                        $scope.$apply();

                    }, 60000);

                });
        }

        function stop() {

        }

        function requestAudioCapturePermissions() {

            log.debug("Requesting Audio Capture Permissions");

            var deferred = $q.defer();

            try {

                // Only check microphone access for speech, we don't need webcam access.
                //var captureSettings = new Windows.Media.Capture.MediaCaptureInitializationSettings();
                //captureSettings.streamingCaptureMode = Windows.Media.Capture.StreamingCaptureMode.audio;
                //captureSettings.mediaCategory = Windows.Media.Capture.MediaCategory.speech;

                capture = new Windows.Media.Capture.MediaCapture();

                capture.initializeAsync(/*captureSettings*/).then(
                    function () {

                        log.debug("Capture initialized");

                        deferred.resolve();
                    },
                    function (error) {
                        // Audio Capture can fail to initialize if there's no audio devices on the system, or if
                        // the user has disabled permission to access the microphone in the Privacy settings.

                        if (error.number == CONST.WIN_MIC_ACCESS_DENIED) {

                            deferred.reject(CONST.ERROR_MIC_ACCESS_DENIED);

                        } else if (error.number == CONST.WIN_MIC_NONE_PRESENT) {

                            deferred.reject(CONST.ERROR_MIC_NONE_PRESENT);

                        } else {

                            log.error(error);
                            deferred.reject(CONST.ERROR_MIC_UNAVAILABLE);

                        }
                    }
                );

            } catch (exception) {
                if (exception.number == CONST.WIN_NO_MEDIA_REGISTERED) { // REGDB_E_CLASSNOTREG
                    deferred.reject(CONST.ERROR_MEDIA_PLAYER_UNAVAILABLE);
                } else {
                    deferred.reject(CONST.ERROR_MIC_UNAVAILABLE);
                }


            }

            return deferred.promise;
        }

    }

})();