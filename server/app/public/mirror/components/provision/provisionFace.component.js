(function () {
    "use strict";

    angular.module("mirrorApp").component("faceProvision", {
        templateUrl: "./components/provision/templates/provisionFace.html",
        controller: ProvisionController,
        controllerAs: "vm",
        bindings: {
            saveFaceClick: '&'
        }
    });

    ProvisionController.$inject = ['facialrecognition.dataservice', 'logger'];

    function ProvisionController(service, log) {

        var vm = this;
        
        vm.personInfo = {
            desiredName: '',
            location: ''
        }
    }

})();