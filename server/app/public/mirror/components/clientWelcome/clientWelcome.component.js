﻿(function () {
    "use strict";

    angular.module("mirrorApp").component("clientWelcome", {
        templateUrl: "./components/clientWelcome/templates/clientWelcome.html",
        controller: ClientWelcomeController,
        controllerAs: "vm",
        bindings: {
            clients: '=',
            refreshRate: '=',
            api: '='
        }
    });

    ClientWelcomeController.$inject = [];

    function ClientWelcomeController() {

        var vm = this;

        vm.welcomeMessage = '';

        vm.$onInit = function () {

            vm.api = {
                updateClientData: updateClientData
            };

            setClientDisplay();
        }

        function updateClientData(clientList) {
            vm.clients = clientList;
            setClientDisplay();
        }

        function setClientDisplay() {
            var clientString = '';
            var lastIndex;
            var now = moment();
            var filteredList;

            moment.tz.add([
                'America/Los_Angeles|PST PDT|80 70|0101|1Lzm0 1zb0 Op0',
                'America/New_York|EST EDT|50 40|0101|1Lz50 1zb0 Op0'
            ]);

            filteredList = vm.clients.filter(function (client) {
                return now >= moment.tz(client.start_time, "America/New_York") && now <= moment.tz(client.end_time, "America/New_York");
            }).map(function (client) {
                return client.client_name;
            });

            if (filteredList.length) {
                filteredList = filteredList.join(', ');
                lastIndex = filteredList.lastIndexOf(',');

                if (lastIndex > 0) {
                    filteredList = filteredList.substring(0, lastIndex) + ' and ' + filteredList.substring(lastIndex + 1);
                }

                vm.welcomeMessage = filteredList;
            }
            else {
                vm.welcomeMessage = '';
            }
        }
    }
})();