﻿(function () {
    'use strict';

    angular.module('mirrorApp').component('videoDisplay', {
        templateUrl: './components/videoDisplay/templates/videodisplay.html',
        controller: VideoDisplayController,
        controllerAs: 'vm',
        bindings: {
            showVideoDisplay: '=',
            detectionInterval: '=',
            api: '=',
            facialDetectionEventName: '=',
            blankMirrorEventName: '='
        }
    });

    VideoDisplayController.$inject = ['$q', '$rootScope', '$scope', 'logger'];

    function VideoDisplayController($q, $rootScope, $scope, log) {

        var vm = this;
        var blankMirrorCount = 0;
        var faceDetectedCount = 0;
        var minFaceThresholds = { 'width': 42, 'height': 42 };
        var faceThresholds = { 'width': 40, 'height': 100 };
        var faceDetected = false;
        var Capture;
        var captureSettings;
        var DeviceEnumeration;
        var displayRequest;
        var effectDefinition;
        var mediaStreamType;
        var mediaCapture;
        var oSystemMediaControls;
        var faceThreshold = 1;
        var Media;
        var cameraInitialized = false;
        
        vm.$onInit = function () {

            vm.api = {
                getMediaCapture: getMediaCapture
            };

            if (typeof Windows != 'undefined') {
                Capture = Windows.Media.Capture;
                captureSettings = new Capture.MediaCaptureInitializationSettings;
                DeviceEnumeration = Windows.Devices.Enumeration;
                displayRequest = new Windows.System.Display.DisplayRequest();
                effectDefinition = new Windows.Media.Core.FaceDetectionEffectDefinition();
                mediaStreamType = Capture.MediaStreamType.videoRecord;
                Media = Windows.Media;
                oSystemMediaControls = Media.SystemMediaTransportControls.getForCurrentView();

                oSystemMediaControls.addEventListener("propertychanged", systemMediaControls_PropertyChanged);

                findCameraDeviceByPanel(DeviceEnumeration.Panel.front).then(function (camera) {

                    if (!camera) {
                        log.error('No camera device found!');
                        return;
                    }

                    mediaCapture = new Capture.MediaCapture();
                    captureSettings.videoDeviceId = camera.id;
                    captureSettings.streamingCaptureMode = Capture.StreamingCaptureMode.video;

                    initializeMediaCapture().then(function (previewUrl) {
                        var preview = document.getElementById('cameraPreview');

                        if (true) {
                            preview.style.transform = 'scale(-1, 1)';
                        }

                        preview.src = previewUrl;
                        preview.play();
                        cameraInitialized = true;
                    });
                },
                function error(e) {
                    console.error(e);
                });
            }
        }

        function getMediaCapture() {
            return mediaCapture;
        }

        function systemMediaControls_PropertyChanged(args) {
            if (!cameraInitialized) {
                vm.$onInit();
            } else if (args.target.soundLevel === Media.SoundLevel.muted) {
                cleanupCamera();
            }
        }

        function findCameraDeviceByPanel(panel) {
            var deferred = $q.defer();
            var deviceInfo;

            DeviceEnumeration.DeviceInformation.findAllAsync(DeviceEnumeration.DeviceClass.videoCapture).then(
                function (devices) {
                    devices.forEach(function (cameraDeviceInfo) {
                        deviceInfo = cameraDeviceInfo;
                        return;
                    });

                    deferred.resolve(deviceInfo);
                },
                function error(e) {
                    log.error(e);
                }
            );

            return deferred.promise;
        }

        function initializeMediaCapture() {
            var deferred = $q.defer();

            mediaCapture.initializeAsync(captureSettings).then(
                function fulfilled(result) {
                    var previewUrl;

                    addFacialRecognition();
                    displayRequest.requestActive();
                    deferred.resolve(URL.createObjectURL(mediaCapture));
                },
                function error(e) {
                    log.error(e);
                }
            );

            return deferred.promise;
        }

        function addFacialRecognition() {
            var deferred = $q.defer();

            mediaCapture.addVideoEffectAsync(effectDefinition, mediaStreamType).done(
                function complete(result) {

                    log.debug("Facial Recognition initialized");

                    result.desiredDetectionInterval = vm.detectionInterval;
                    result.addEventListener('facedetected', handleFaces);
                    deferred.resolve();
                },
                function error(e) {
                    log.error(e);
                }
            );

            return deferred.promise;
        }

        function handleFaces(args) {
            var detectedFaces = args.resultFrame.detectedFaces;
            var numFaces = detectedFaces.length;
            var face;
            var sufficientDimensions = false;

            if (numFaces > 0) {

                faceDetectedCount++;

                if (faceDetectedCount >= faceThreshold) {

                    for (var i = 0; i < numFaces; i++) {
                        face = detectedFaces.getAt(i).faceBox;

                        if (face.width > minFaceThresholds.width && face.height > minFaceThresholds.height) {
                            blankMirrorCount = 0;
                            faceDetectedCount = 0;
                            $rootScope.$broadcast(vm.facialDetectionEventName, { mediaCapture: mediaCapture, numFaces: numFaces });
                        }
                    }
                }
            }
            else {
                blankMirrorCount = 0;
                faceDetectedCount = 0;
                $rootScope.$broadcast(vm.blankMirrorEventName);
            }
        }

        function cleanupCamera() {
            var promiseList = {};

            if (cameraInitialized) {
                var previewVidTag = document.getElementById("cameraPreview");

                previewVidTag.pause();
                previewVidTag.src = null;
                displayRequest.requestRelease();
                cameraInitialized = false;
            }
        }

        function activateCamera(previewUrl) {
            var preview = document.getElementById('cameraPreview');

            if (true) {
                preview.style.transform = 'scale(-1, 1)';
            }

            preview.src = previewUrl;
            preview.play();
        }
        
    }
})();