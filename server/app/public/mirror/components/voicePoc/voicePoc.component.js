(function () {
    "use strict";

    angular.module("mirrorApp")

        .component("voicePoc", {
            templateUrl: "./components/voicePOC/templates/voicePOC.html",
            controller: VoicePocController,
            controllerAs: "vm",
            bindings: {
                commands: '=',
                voiceEvent: '&'
            }
        });

    VoicePocController.$inject = ["$scope", "CONST"];

    function VoicePocController($scope, CONST) {

        var vm = this;

        vm.show = false;
        vm.dictation = "";
        vm.log = "";

        $scope.$on(CONST.EVENT_VOICE_RESULT_GENERATED, handleDictationEvent);
        $scope.$on(CONST.COMMAND_HIDE_DICTATION_LOG, handleHideDictationEvent);
        $scope.$on(CONST.COMMAND_SHOW_DICTATION_LOG, handleShowDictationEvent);
        $scope.$on(CONST.COMMAND_TOGGLE_DICTATION_LOG, handleToggleDictationEvent);

        $scope.$on(CONST.EVENT_LOG_DEBUG, handleLogEvent);

        //////////

        vm.$onInit = function () {
            
        };

        function log(msg) {
            vm.log = msg + "\n" + vm.log;
        }

        function handleLogEvent(event, msg){
            log(msg);
        }

        function handleDictationEvent(event, dictation) {
            vm.dictation = dictation;

            log(dictation);

            $scope.$apply();
        }

        function handleHideDictationEvent() {
            vm.show = false;
            $scope.$apply();
        }

        function handleShowDictationEvent() {
            vm.show = true;
            $scope.$apply();
        }

        function handleToggleDictationEvent() {
            vm.show = !vm.show;
            $scope.$apply();
        }

    }

})();