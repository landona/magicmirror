(function () {
    'use strict';

    angular.module('mirrorApp')
        .factory('voicePocService', ['$q', '$rootScope', 'CONST', 'logger', voicePocService]);

    function voicePocService($q, $rootScope, CONST, log) {

        var recognizer;
        var context;
        var resourceMap;

        var service = {};

        activate();

        return service;

        //////////

        function activate() {

            log.debug("initializing speech recognition");

            var deferred = $q.defer();

            requestAudioCapturePermissions().then(
                function () {

                    var defaultLang = Windows.Media.SpeechRecognition.SpeechRecognizer.systemSpeechLanguage;

                    var rcns = Windows.ApplicationModel.Resources.Core;
                    context = new rcns.ResourceContext();
                    context.languages = new Array(defaultLang.languageTag);
                    resourceMap = rcns.ResourceManager.current.mainResourceMap.getSubtree(CONST.PROP_LOCALIZATION_SPEECH_RESOURCES);

                    initializeRecognizer(defaultLang).then(function () {

                        initiateDictation();

                    });

                    deferred.resolve();

                },
                function (error) {
                    deferred.reject(error);
                }
            );

            return deferred.promise;

        }

        function initiateDictation() {

            if (recognizer && recognizer.state != Windows.Media.SpeechRecognition.SpeechRecognizerState.idle) {
                log.error(CONST.ERROR_INITIATE_FAILED);

                return stopDictation();
            }

            dictateAdInfinitum();
        }
        
        function stopDictation() {
            if (recognizer && recognizer.state != Windows.Media.SpeechRecognition.SpeechRecognizerState.idle) {
                log.debug(CONST.DEBUG_STOPPING_DICTATION);
                recognizer.continuousRecognitionSession.stopAsync();
            }
        }

        function dictateAdInfinitum(eventArgs) {
            if (eventArgs && eventArgs.status != Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.success) {
                speechRecognizerUnsuccessful(eventArgs.status);
            }

            // Start the continuous recognition session. Results are handled in the event handlers below.
            try {
                recognizer.continuousRecognitionSession.startAsync();
            }
            catch (e) {
                log.debug(e);
            }
        }

        function requestAudioCapturePermissions() {

            var deferred = $q.defer();

            try {

                // Only check microphone access for speech, we don't need webcam access.
                var captureSettings = new Windows.Media.Capture.MediaCaptureInitializationSettings();
                captureSettings.streamingCaptureMode = Windows.Media.Capture.StreamingCaptureMode.audio;
                captureSettings.mediaCategory = Windows.Media.Capture.MediaCategory.speech;

                var capture = new Windows.Media.Capture.MediaCapture();

                capture.initializeAsync(captureSettings).then(
                    function () {
                        deferred.resolve();
                    },
                    function (error) {
                        // Audio Capture can fail to initialize if there's no audio devices on the system, or if
                        // the user has disabled permission to access the microphone in the Privacy settings.

                        if (error.number == CONST.WIN_MIC_ACCESS_DENIED) {

                            deferred.reject(CONST.ERROR_MIC_ACCESS_DENIED);

                        } else if (error.number == CONST.WIN_MIC_NONE_PRESENT) {

                            deferred.reject(CONST.ERROR_MIC_NONE_PRESENT);

                        } else {

                            log.error(error);
                            deferred.reject(CONST.ERROR_MIC_UNAVAILABLE);

                        }
                    }
                );

            } catch (exception) {
                if (exception.number == CONST.WIN_NO_MEDIA_REGISTERED) { // REGDB_E_CLASSNOTREG
                    deferred.reject(CONST.ERROR_MEDIA_PLAYER_UNAVAILABLE);
                } else {
                    deferred.reject(CONST.ERROR_MIC_UNAVAILABLE);
                }


            }

            return deferred.promise;
        }



        function initializeRecognizer(language) {

            var deferred = $q.defer();

            if (typeof recognizer !== 'undefined') {
                recognizer = null;
            }
            recognizer = Windows.Media.SpeechRecognition.SpeechRecognizer(language);

            // Provide feedback to the user about the state of the recognizer.
            recognizer.addEventListener(CONST.EVENT_VOICE_STATE_CHANGED, onSpeechRecognizerStateChanged, false);

            // Handle continuous recognition events. Completed fires when various error states occur or the session otherwise ends.
            // ResultGenerated fires when recognized phrases are spoken or the garbage rule is hit.
            recognizer.continuousRecognitionSession.addEventListener(CONST.EVENT_VOICE_RESULT_GENERATED, onSpeechRecognizerResultGenerated, false);
            recognizer.continuousRecognitionSession.addEventListener(CONST.EVENT_VOICE_COMPLETED, dictateAdInfinitum, false);

            // Compile the dictation topic constraint, which optimizes for dictated speech.
            var dictationConstraint = new Windows.Media.SpeechRecognition.SpeechRecognitionTopicConstraint(Windows.Media.SpeechRecognition.SpeechRecognitionScenario.dictation, CONST.PROP_CONSTRAINT_DICTATION);
            recognizer.constraints.append(dictationConstraint);

            recognizer.compileConstraintsAsync().done(
                function (result) {
                    // Check to make sure that the constraints were in a proper format and the recognizer was able to compile them.
                    if (result.status != Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.success) {

                        // Let the user know that the grammar didn't compile properly.
                        speechRecognizerUnsuccessful(result.status);

                        deferred.reject(CONST.ERROR_VOICE_GRAMMAR_CONSTRAINTS_FAILED);
                    }

                    deferred.resolve();
                },
                function (error) {
                    log.error(CONST.ERROR_VOICE_GRAMMAR_CONSTRAINTS_FAILED);
                    log.error(error);
                    deferred.reject(error);
                }
            );

            return deferred.promise;
        }



        function onSpeechRecognizerStateChanged(eventArgs) {
            /// <summary>
            /// Looks up the state text and displays the message to the user.
            /// </summary>
            switch (eventArgs.state) {
                case Windows.Media.SpeechRecognition.SpeechRecognizerState.idle: {
                    log.debug(CONST.DEBUG_VOICE_IDLE);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognizerState.capturing: {
                    log.debug(CONST.DEBUG_VOICE_CAPTURING);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognizerState.processing: {
                    log.debug(CONST.DEBUG_VOICE_PROCESSING);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognizerState.soundStarted: {
                    log.debug(CONST.DEBUG_VOICE_SOUND_STARTED);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognizerState.soundEnded: {
                    log.debug(CONST.DEBUG_VOICE_SOUND_ENDED);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognizerState.speechDetected: {
                    log.debug(CONST.DEBUG_VOICE_SPEECH_DETECTED);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognizerState.paused: {
                    log.debug(CONST.DEBUG_VOICE_SPEECH_PAUSED);

                    break;
                }
                default: {
                    break;
                }
            }
        }

        function onSpeechRecognizerResultGenerated(eventArgs) {
            /// <summary>
            /// Handle events fired when a result is generated. Check for high to medium confidence, and then append the
            /// string to the end of the previous results. Replace the content of the textbox with the resulting string to
            /// remove any hypothesis text that may be present.
            /// </summary>
            if (eventArgs.result.confidence == Windows.Media.SpeechRecognition.SpeechRecognitionConfidence.high ||
                eventArgs.result.confidence == Windows.Media.SpeechRecognition.SpeechRecognitionConfidence.medium) {

                log.debug(eventArgs.result.text);

                $rootScope.$broadcast(CONST.EVENT_VOICE_RESULT_GENERATED, eventArgs.result.text);
                $rootScope.$broadcast('EVENT_PUB_NUB_MSG', eventArgs.result.text);

                parseCommand(eventArgs.result.text);

            }
            else {
                if (eventArgs.result.text.length != 0) {
                    log.debug(CONST.DEBUG_VOICE_REJECTED_LOW_CONFIDENCE + eventArgs.result.text);
                }
            }

        }

        function speechRecognizerUnsuccessful(resultStatus) {
            /// <summary>
            /// Looks up the error text and displays the message to the user.
            /// </summary>
            switch (resultStatus) {
                case Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.audioQualityFailure: {
                    log.error(CONST.ERROR_VOICE_AUDIO_QUALITY_FAILURE);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.grammarCompilationFailure: {
                    log.error(CONST.ERROR_VOICE_GRAMMAR_COMPILATION_FAILURE);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.grammarLanguageMismatch: {
                    log.error(CONST.ERROR_VOICE_GRAMMAR_LANGUAGE_MISMATCH);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.microphoneUnavailable: {
                    log.error(CONST.ERROR_VOICE_MIC_UNAVAILABLE);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.networkFailure: {
                    log.error(CONST.ERROR_VOICE_NETWORK_FAILURE);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.pauseLimitExceeded: {
                    log.error(CONST.ERROR_VOICE_PAUSE_LIMIT_EXCEEDED);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.timeoutExceeded: {
                    log.error(CONST.ERROR_VOICE_TIMEOUT_EXCEEDED);


                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.topicLanguageNotSupported: {
                    log.error(CONST.ERROR_VOICE_TOPIC_LANGUAGE_NOT_SUPPORTED);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.unknown: {
                    log.error(CONST.ERROR_VOICE_UNKNOWN);

                    break;
                }
                case Windows.Media.SpeechRecognition.SpeechRecognitionResultStatus.userCanceled: {
                    log.error(CONST.ERROR_VOICE_CANCELED);

                    break;
                }
                default: {
                    log.error(CONST.ERROR_TOTAL_DISASTER);
                    break;
                }
            }
        }

        function parseCommand(command) {

            log.info("Parsing Command");

            if (command.match(/^.*\b(my|i|hide|high|hi|hyper|hike|height|find)\b.*weather|whether.*$/i)) {
                log.info("Hiding the weather");
                $rootScope.$broadcast(CONST.COMMAND_HIDE_WEATHER);  
            }

            else if (command.match(/^.*\b(what|what\'s|the)\b.*weather|whether.*$/i)) {
                log.info("change the weather");
                $rootScope.$broadcast("ChangeWeather", { command: command }); 
            }

            else if (command.match(/^.*show.*weather.*$/i)) {
                log.info("Showing the weather");
                $rootScope.$broadcast(CONST.COMMAND_SHOW_WEATHER);
            }

            else if (command.match(/^.*\b(my|i|hide|high|hi|hyper|hike|height|find)\b.*ation|dictate|Decatur|d'acacia.*$/i)) {
                log.info("Hiding dictation");
                $rootScope.$broadcast(CONST.COMMAND_HIDE_DICTATION_LOG);
            }

            else if (command.match(/^.*show.*ation|dictate|Decatur|d'acacia.*$/i)) {
                log.info("Showing the dictation");
                $rootScope.$broadcast(CONST.COMMAND_SHOW_DICTATION_LOG);
            }

            else if (command.match(/^.*show.*traffic.*$/i)) {
                log.info("Showing the traffic map");
                $rootScope.$broadcast('ShowTrafficMap');
            }

            else if (command.match(/^.*\b(my|i|hide|high|hi|hyper|hike|height|find|how)\b.*traffic.*$/i)) {
                log.info("Hide the traffic map");
                $rootScope.$broadcast('HideTrafficMap');
            }

            else {
                log.info("No command match");
            }

        }

    }

})();