(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name logger
     * @description
     * Simple logging implementation, currently wraps the built in ng $log
     * service.
     * The native service was wrapped so that the logging implementation can be swapped
     * in the future, if desired. 
     */
    angular.module('mirrorApp')
        .factory('logger', ['$log', '$rootScope', 'CONST', Logger]);

    function Logger($log, $rootScope, CONST) {

        var service = {
            log: log,
            info: info,
            warn: warn,
            error: error,
            debug: debug
        }

        return service;

        //////////

        function log(msg) {
            $log.log(msg);
            $rootScope.$broadcast(CONST.EVENT_LOG_LOG, msg);
        }

        function info(msg) {
            $log.info(msg);
            $rootScope.$broadcast(CONST.EVENT_LOG_INFO, msg);
        }

        function warn(msg) {
            $log.warn(msg);
            $rootScope.$broadcast(CONST.EVENT_LOG_WARN, msg);
        }

        function error(msg) {
            $log.error(msg);
            $rootScope.$broadcast(CONST.EVENT_LOG_ERROR, msg);
        }

        function debug(msg) {
            console.log(msg);

            $log.debug(msg);
            $rootScope.$broadcast(CONST.EVENT_LOG_DEBUG, msg);
        }
    }

})();