﻿(function () {
    'use strict';

    angular.module('mirrorApp')
        .factory('session-storage', ['$window', SessionStorage]);

    /**
     * @ngdoc service
     * @name session-storage
     * @description
     * Simple session management service, currently wraps the ngStorage.
     * The ngStorage service was wrapped so that the session storage implementation can be swapped
     * in the future, if desired. 
     */
    function SessionStorage($window) {
        return {
            get: get,
            set: set
        };

        /**
         * @ngdoc function
         * @name get
         * @param {string} key - Name of the session value to return.
         * @description
         * Returns the session value based on the key passed into the function. 
         */
        function get(key) {
            return JSON.parse($window.sessionStorage.getItem(key));
        }

        /**
         * @ngdoc function
         * @name set
         * @param {string} key - Name of the session value to set.
         * @param {string} data - Value to be stored in session.
         * @description
         * Stores the data value in session with a name matching the key.
         */
        function set(key, data) {
            $window.sessionStorage.setItem(key, JSON.stringify(data));
        }
    }

})();