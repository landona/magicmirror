(function () {
    "use strict";
    
    angular.module("mirrorApp")
        .factory("keyboardService", ["$document", "$rootScope", KeyboardService]);

    function KeyboardService($document, $rootScope) {

        $document.bind('keypress', function (e) {
            $rootScope.$broadcast('keypress', e);
        });

        return {};
    }

})();