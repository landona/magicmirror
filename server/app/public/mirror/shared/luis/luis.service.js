﻿(function () {

    angular.module('mirrorApp')
        .factory('luis.dataservice', ['$http', 'logger', LuisDataService]);

    function LuisDataService($http, logger) {
        return {
            getWeatherIntent: getWeatherIntent,
        };

        function getWeatherIntent(intent) {
            var luisUrl = 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/ec62dbbc-1dac-40f7-931f-3b14562b6b24?subscription-key=765ee366cff4439795286b464eb49cb2&timezoneOffset=0.0&verbose=true&q=' + intent;
            
            return $http.get(luisUrl)
                .then(responseData)
                .catch(serviceError);
        }

        function getBotIntent(intent) {
            var luisUrl = 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/77e9b852-5e53-47d1-b41b-e0eac7521062?subscription-key=765ee366cff4439795286b464eb49cb2&verbose=true&timezoneOffset=0&q=' + intent;

            return $http.get(luisUrl)
                .then(responseData)
                .catch(serviceError);
        }

        function responseData(response) {
            return response.data;
        }

        function serviceError(error) {
            logger.error(error.data);
        }
    }

})();