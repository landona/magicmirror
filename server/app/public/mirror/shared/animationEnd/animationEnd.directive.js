(function () {
    "use strict";

    angular.module("mirrorApp")
        .directive("animationEnd", ['$animate', AnimationEnd]);

    function AnimationEnd($animate) {

        var directive = {
            scope: {
                hideContent: '=',
                hideContentCallback: '='
            },
            link: link,
            restrict: 'A'
        };

        return directive;

        //////////

        function link(scope, element, attrs) {

            scope.$watch('hideContent', function (newValue, oldValue) {

                if (newValue) {
                    $animate.addClass(element, 'jl-fade').then(
                        function () {
                            scope.hideContentCallback();
                            $animate.removeClass(element, 'jl-fade');
                        }
                    );
                }

            });

        }

    }

})();