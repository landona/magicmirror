module.exports = function () {
    
    var CONFIG = {
        BASE: "app",
        DIST: "dist",
        DOCS: "docs",

        LIB_JS: [
            "./node_modules/angular/angular.min.js",
            "./node_modules/angular-aria/angular-aria.min.js",
            "./node_modules/angular-animate/angular-animate.min.js",
            "./node_modules/angular-material/angular-material.min.js",
            "./node_modules/moment/min/moment.min.js",
            "./node_modules/moment-timezone/builds/moment-timezone.min.js",
            "./node_modules/pubnub/dist/web/pubnub.min.js",
            "./node_modules/pubnub-angular/dist/pubnub-angular.min.js"
        ],

        LIB_CSS: [
            "./node_modules/angular-material/angular-material.min.css"
        ],

        LIB_FONT: [
            "./node_modules/material-design-icons-iconfont/dist/fonts/material-icons.css",
            "./node_modules/material-design-icons-iconfont/dist/fonts/MaterialIcons-Regular.eot",
            "./node_modules/material-design-icons-iconfont/dist/fonts/MaterialIcons-Regular.ijmap",
            "./node_modules/material-design-icons-iconfont/dist/fonts/MaterialIcons-Regular.svg",
            "./node_modules/material-design-icons-iconfont/dist/fonts/MaterialIcons-Regular.ttf",
            "./node_modules/material-design-icons-iconfont/dist/fonts/MaterialIcons-Regular.woff",
            "./node_modules/material-design-icons-iconfont/dist/fonts/MaterialIcons-Regular.woff2"
        ]
    };

    var CONST = {
        BUILD_DESC : "Builds the gulp nodejs server and deploys to the " + CONFIG.DIST + " directory.",
        BUILD_OPTIONS: {
            "iis": "Moves the web.config and package.json files to the dist directory.",
            "azure" : "Assigns azure build defaults (iis)"
        },
        BUILD_DOCS_DESC: "Generates jsdoc style api documentation and places it in the " + CONFIG.DOCS + " directory.",
        SERVE_DOCS_DESC: "Generates and serves api documentation. Server port is displayed in console on launch.",
        SYNC_DESC: "Syncronizes angular app in the distribution w/ the source version in real time"
    }

    return {
        CONFIG: CONFIG,
        CONST: CONST
    }
}