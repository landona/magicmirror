@if "%SCM_TRACE_LEVEL%" NEQ "4" @echo off

:: ----------------------
:: KUDU Deployment Script
:: Version: 1.0.9
:: ----------------------

:: Prerequisites
:: -------------

::-----------------------
:: CUSTOM VARIABLES
::-----------------------
SET BASE_SOURCE=\server
SET BASE_DIST=\server\dist

:: Verify node.js installed
where node 2>nul >nul
IF %ERRORLEVEL% NEQ 0 (
  echo Missing node.js executable, please install node.js, if already installed make sure it can be reached from current environment.
  goto error
)

:: Setup
:: -----

setlocal enabledelayedexpansion

SET ARTIFACTS=%~dp0%..\artifacts

IF NOT DEFINED DEPLOYMENT_SOURCE (
  SET DEPLOYMENT_SOURCE=%~dp0%%BASE_SOURCE%
)

SET DEPLOYMENT_SOURCE=%DEPLOYMENT_SOURCE%%BASE_SOURCE%

echo deployment source
echo %DEPLOYMENT_SOURCE%

IF NOT DEFINED DEPLOYMENT_DIST (
  SET DEPLOYMENT_DIST=%~dp0%
)

SET DEPLOYMENT_DIST=%DEPLOYMENT_DIST%%BASE_DIST%

echo deployment source
echo %DEPLOYMENT_DIST%

IF NOT DEFINED DEPLOYMENT_TARGET (
  SET DEPLOYMENT_TARGET=%ARTIFACTS%\wwwroot
)

IF NOT DEFINED NEXT_MANIFEST_PATH (
  SET NEXT_MANIFEST_PATH=%ARTIFACTS%\manifest

  IF NOT DEFINED PREVIOUS_MANIFEST_PATH (
	SET PREVIOUS_MANIFEST_PATH=%ARTIFACTS%\manifest
  )
)

IF NOT DEFINED KUDU_SYNC_CMD (
  :: Install kudu sync
  echo Installing Kudu Sync
  call npm install kudusync -g --silent
  IF !ERRORLEVEL! NEQ 0 goto error

  :: Locally just running "kuduSync" would also work
  SET KUDU_SYNC_CMD=%appdata%\npm\kuduSync.cmd
)
goto Deployment


::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Deployment
:: ----------

:Deployment
echo Handling node.js deployment.

echo ----------------------------------
echo KUDU_SYNC_CMD: %KUDU_SYNC_CMD%
echo DEPLOYMENT_SOURCE: %DEPLOYMENT_SOURCE%
echo DEPLOYMENT_DIST: %DEPLOYMENT_DIST%
echo DEPLOYMENT_TARGET: %DEPLOYMENT_TARGET%
echo NEXT_MANIFEST_PATH: %NEXT_MANIFEST_PATH%
echo PREVIOUS_MANIFEST_PATH: %PREVIOUS_MANIFEST_PATH%
echo KUDU_SELECT_NODE_VERSION_CMD: %KUDU_SELECT_NODE_VERSION_CMD%
echo DEPLOYMENT_TEMP: %DEPLOYMENT_TEMP%

:: 1. Select node version
IF NOT DEFINED NODE_EXE (
SET NODE_EXE=node
)

SET NPM_CMD="!NODE_EXE!" "!NPM_JS_PATH!"

:: 2. Compile the Source
IF EXIST "%DEPLOYMENT_SOURCE%\package.json" (
  pushd "%DEPLOYMENT_SOURCE%"
  
  call :ExecuteCmd !NPM_CMD! install 
  IF !ERRORLEVEL! NEQ 0 goto error

  ::RUN CUSTOM GULP COMPILE COMMAND
  echo Running gulp build --azure
  call :ExecuteCmd gulp build --azure 
  IF !ERRORLEVEL! NEQ 0 goto error

  popd
)
 
:: 3. KuduSync
IF /I "%IN_PLACE_DEPLOYMENT%" NEQ "1" (
  call :ExecuteCmd "%KUDU_SYNC_CMD%" -v 50 -f "%DEPLOYMENT_DIST%" -t "%DEPLOYMENT_TARGET%" -n "%NEXT_MANIFEST_PATH%" -p "%PREVIOUS_MANIFEST_PATH%" -i ".git;.hg;.deployment;deploy.cmd"
  IF !ERRORLEVEL! NEQ 0 goto error
) 

:: 4. Install server dependencies
IF EXIST "%DEPLOYMENT_TARGET%\package.json" (
	pushd "%DEPLOYMENT_TARGET%"

	call :ExecuteCmd !NPM_CMD! install --only=production
	IF !ERRORLEVEL! NEQ 0 goto error

	popd
)

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto end

:: Execute command routine that will echo out when error
:ExecuteCmd
setlocal
set _CMD_=%*
call %_CMD_%
if "%ERRORLEVEL%" NEQ "0" echo Failed exitCode=%ERRORLEVEL%, command=%_CMD_%
exit /b %ERRORLEVEL%

:error
endlocal
echo An error has occurred during web site deployment.
call :exitSetErrorLevel
call :exitFromFunction 2>nul

:exitSetErrorLevel
exit /b 1

:exitFromFunction
()

:end
endlocal
echo Finished successfully.
