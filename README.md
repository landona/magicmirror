# The Slalom Hartford Magic Mirror #

TL;DR;

    git clone https://JaredLinde@bitbucket.org/slalomhartford/magicmirror.git
    cd magicmirror/server
    npm install
    gulp build
    npm start

This will install and run the magic mirror server locally. You can access the application via a web browser or by launching the universal windows project (client) from Visual Studio. Note that hardware access is limited to the UWP wrapper and will not work in a standard web browser.

More to Come...